#include "renderablerotationindicator.h"
#include <QColor>

#define INDICATOR_SIZE      25.0f
#define BACKGROUND_SIZE     (INDICATOR_SIZE * 1.5f)

RenderableRotationIndicator::RenderableRotationIndicator():
    RenderableObject(1,1)
{
    QVector<RenderableObject::VertexData> vertexData;

    //Indicator vertices
    const QVector4D xAxisColor  = colorToVector4D(Qt::red);
    const QVector4D yAxisColor  = colorToVector4D(Qt::green);
    const QVector4D zAxisColor  = colorToVector4D(Qt::blue);
    vertexData.append({QVector3D(0, 0, 0), xAxisColor});
    vertexData.append({QVector3D(1, 0, 0), xAxisColor});
    vertexData.append({QVector3D(0, 0, 0), yAxisColor});
    vertexData.append({QVector3D(0, 1, 0), yAxisColor});
    vertexData.append({QVector3D(0, 0, 0), zAxisColor});
    vertexData.append({QVector3D(0, 0, 1), zAxisColor});
    int indicatorVerticesCount = vertexData.size();

    createVertexBuffer(vertexData.size(), vertexData.data());

    RenderingConfig & indicatorRndrConfRef  = getRndrConfRef(0);
    indicatorRndrConfRef.drawMode           = RenderingConfig::DrawMode_Lines;
    indicatorRndrConfRef.firstVertex        = 0;
    indicatorRndrConfRef.vertexCount        = indicatorVerticesCount;
    indicatorRndrConfRef.useIndices         = false;
    indicatorRndrConfRef.colorMode          = RenderingConfig::ColorMode_DirectColor;
    indicatorRndrConfRef.lineWidth          = 2.0f;
    indicatorRndrConfRef.mMatrixIndex       = 0;
    indicatorRndrConfRef.colorAFactor       = QVector4D(1.0f,1.0f,1.0f,1.0f);
    indicatorRndrConfRef.colorBFactor       = QVector4D(0.0f,0.0f,0.0f,0.0f);
}

void RenderableRotationIndicator::updatePositionAndRotation(QVector2D viewPortSize, QQuaternion rotation)
{
    QVector3D position = QVector3D(viewPortSize.x() / 2 - BACKGROUND_SIZE,
                                   -viewPortSize.y() / 2 + BACKGROUND_SIZE,
                                   0.0f);
    getMMatrixRef(0) = generateMMatrix(position, rotation, INDICATOR_SIZE);
}
