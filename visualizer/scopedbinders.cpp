#include "scopedbinders.h"


//##############################################################################

ScopedAttributeArrayBinder::ScopedAttributeArrayBinder() :
    m_program(nullptr)
{

}

ScopedAttributeArrayBinder::ScopedAttributeArrayBinder(QOpenGLShaderProgram *program, const char * name,
                                                               GLenum type, int offset, int tupleSize, int stride):
    m_program(program)
{
    m_attributeNumber = m_program->attributeLocation(name);
    m_program->enableAttributeArray(m_attributeNumber);
    m_program->setAttributeBuffer(m_attributeNumber,  // GLSL attribute
                                  type,               // type
                                  offset,             // position in VBO
                                  tupleSize,          // number of components
                                  stride);            // size until next value
}

ScopedAttributeArrayBinder::~ScopedAttributeArrayBinder()
{
    if(m_program)
    {
        m_program->disableAttributeArray(m_attributeNumber);
    }
}

//##############################################################################

ScopedVertexBufferBinder::ScopedVertexBufferBinder(QOpenGLBuffer *bufferToBind) :
    m_buffer(nullptr)
{
    if(bufferToBind && bufferToBind->isCreated())
    {
        m_buffer = bufferToBind;
        m_buffer->bind();
    }
}

ScopedVertexBufferBinder::~ScopedVertexBufferBinder()
{
    if(m_buffer)
    {
        m_buffer->release();
    }
}

bool ScopedVertexBufferBinder::isBindingSuccess()
{
    return (m_buffer != nullptr);
}

//##############################################################################


ScopedTextureBinder::ScopedTextureBinder(QOpenGLShaderProgram *program, const char * name,
                                                           QOpenGLTexture *texture):
    m_texture(nullptr)
{
    if(texture && texture->isCreated())
    {
        m_texture = texture;
        m_texture->bind(0);
        program->setUniformValue(name,0);
    }
}

ScopedTextureBinder::~ScopedTextureBinder()
{
    if(m_texture)
    {
        m_texture->release(0);
    }
}
