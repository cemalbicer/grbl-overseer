#ifndef VISUALIZERWIDGET_H
#define VISUALIZERWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QMouseEvent>
#include <QVector3D>
#include <QVector>

class RenderableObject;

class VisualizerWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    explicit VisualizerWidget(QWidget *parent = 0);
    ~VisualizerWidget();

signals:

public slots:
    //Quick test hack, must rethink this !!!
    void addObjectToRender(RenderableObject* object)
    {
        m_objectsToRender.append(object);
    }

protected:
    void mouseDoubleClickEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
    void wheelEvent(QWheelEvent *e) Q_DECL_OVERRIDE;

    void initializeGL() Q_DECL_OVERRIDE;
    void resizeGL(int w, int h) Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;

    void initShaders();

    void resetView();

private:
    QOpenGLShaderProgram* m_program;

    QVector2D m_mousePrevPosition;

    QVector3D m_translation;
    QQuaternion m_rotation;

    QMatrix4x4 m_viewMatrix;
    void updateViewMatrix();    //Recompute view matrix. Call it to apply modifications to m_translation or m_rotation

    QMatrix4x4 m_projMatrix;

    QVector<RenderableObject*> m_objectsToRender;
};

#endif // VISUALIZERWIDGET_H
