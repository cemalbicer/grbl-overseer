#include "visualizerwidget.h"
#include "renderedobjects.h"

#include "QRegularExpression"
#include <QtMath>

#define ZNEAR               5.0f
#define ZFAR                10000.0f
#define VIEW_DIST_MIN       20.0f
#define VIEW_DIST_MAX       9000.0f
#define VIEW_DIST_DEFAULT   250.0f


VisualizerWidget::VisualizerWidget(QWidget *parent) :
    QOpenGLWidget(parent)
{

}


void VisualizerWidget::mouseDoubleClickEvent(QMouseEvent *e){
    Q_UNUSED(e);
    resetView();
}

void VisualizerWidget::mousePressEvent(QMouseEvent *e){
    m_mousePrevPosition = QVector2D(e->localPos());
}



void VisualizerWidget::mouseMoveEvent(QMouseEvent *e)
{
    QVector2D diff = QVector2D(e->localPos()) - m_mousePrevPosition;
    m_mousePrevPosition = QVector2D(e->localPos());

    if(e->buttons() == Qt::LeftButton ){
        // Rotation axis is perpendicular to the mouse position difference
        // vector
        QVector3D n = QVector3D(diff.y(), diff.x(), 0.0).normalized();

        // Calculate new rotation axis as weighted sum
        QVector3D rotationAxis = n.normalized();

        m_rotation = QQuaternion::fromAxisAndAngle(rotationAxis, diff.length()) * m_rotation;

    }

    if(e->buttons() == Qt::RightButton ){
        float ratio =10.0f;
        m_translation[0] += diff.x() / ratio;
        m_translation[1] -= diff.y() / ratio;
    }

    // Request an update
    updateViewMatrix();
    update();
}

void VisualizerWidget::wheelEvent(QWheelEvent *e){
    float distanceModifier = -(e->angleDelta().y());

    //Always multiply / divide distance
    float newZvalue = m_translation.z() *qPow(1.001,distanceModifier);

    //Ensure we never goes outside [VIEW_DIST_MIN;VIEW_DIST_MAX]
    m_translation.setZ( qBound(-VIEW_DIST_MAX, newZvalue, -VIEW_DIST_MIN));
    updateViewMatrix();
    update();
}


void VisualizerWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_MULTISAMPLE);

    //Allows for some transparency rendering, quite poor, but does the job without sorting
    glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);

    initShaders();

    resetView();
}

void VisualizerWidget::initShaders()
{
    m_program = new QOpenGLShaderProgram(this);

    // Compile vertex shader
    if (!m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vshader"))
        close();

    // Compile fragment shader
    if (!m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fshader"))
        close();

    // Link shader pipeline
    if (!m_program->link())
        close();

    // Bind shader pipeline for use
    if (!m_program->bind())
        close();
}

void VisualizerWidget::resetView()
{
    m_translation = QVector3D(0,0,-VIEW_DIST_DEFAULT);
    m_rotation = QQuaternion();
    updateViewMatrix();
    update();
}

void VisualizerWidget::updateViewMatrix()
{
    m_viewMatrix.setToIdentity();
    m_viewMatrix.translate(m_translation);
    m_viewMatrix.rotate(m_rotation);
}




void VisualizerWidget::resizeGL(int w, int h){
    /*
    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 0.0, far plane to 100.0, field of view 45 degrees
    const qreal zNear = ZNEAR, zFar = ZFAR, fov = 45.0;

    // Reset projection
    m_projection.setToIdentity();

    // Set perspective projection
    m_projection.perspective(fov, aspect, zNear, zFar);
    */


    float width = 800.0f;
    float height = 800.0f;

    width/=2;
    height/=2;
    if(w>h){
        float ratio = (float)w/(float)(h>0 ? h : 1);
        width*=ratio;
    }
    else if(h>w){
        float ratio = (float)h/(float)(w>0 ? w  : 1);
        height*=ratio;
    }

    m_projMatrix.setToIdentity();
    m_projMatrix.ortho(-width,width,-height,height, ZNEAR, ZFAR);
}

void VisualizerWidget::paintGL()
{
    // Clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Setup view & projection matrix in program
    m_program->setUniformValue("p_matrix",m_projMatrix);
    m_program->setUniformValue("v_matrix",m_viewMatrix);

    foreach(RenderedObject* object, m_objectsToRender)
    {
        object->render(m_program);
    }
}



VisualizerWidget::~VisualizerWidget()
{

}

