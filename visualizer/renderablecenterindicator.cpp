#include "renderablecenterindicator.h"
#include <QColor>

#define INDICATOR_SIZE  2

RenderableCenterIndicator::RenderableCenterIndicator():
    RenderableObject(1, 1)
{
    QVector4D color = colorToVector4D(Qt::white);

    QVector<RenderableObject::VertexData> vertexData;
    vertexData.append({QVector3D(-INDICATOR_SIZE,-INDICATOR_SIZE  ,0), color});
    vertexData.append({QVector3D(-INDICATOR_SIZE,+INDICATOR_SIZE  ,0), color});
    vertexData.append({QVector3D(+INDICATOR_SIZE,-INDICATOR_SIZE  ,0), color});
    vertexData.append({QVector3D(+INDICATOR_SIZE,+INDICATOR_SIZE  ,0), color});

    createVertexBuffer(vertexData.size(), vertexData.data());

    RenderingConfig & mainRndrConfPtr = getRndrConfRef(0);
    mainRndrConfPtr.drawMode        = RenderingConfig::DrawMode_TriangleStrip;
    mainRndrConfPtr.vertexCount     = vertexData.size();
    mainRndrConfPtr.useIndices      = false;
    mainRndrConfPtr.colorMode       = RenderingConfig::ColorMode_DirectColor;
    mainRndrConfPtr.lineWidth       = 2.0f;
    mainRndrConfPtr.mMatrixIndex    = 0;
    mainRndrConfPtr.colorAFactor    = QVector4D(1.0f,1.0f,1.0f,1.0f);
    mainRndrConfPtr.colorBFactor    = QVector4D(0.0f,0.0f,0.0f,0.0f);
}
