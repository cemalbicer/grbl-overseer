#ifndef VISUALIZERITEM_H
#define VISUALIZERITEM_H

#include "visualizerevent.h"
#include "visualizeritemrenderer.h"
#include "touchpointsmanager.h"

#include <grbl/grblboard.h>
#include <grbl/grblstatusiface.h>
#include <grbl/grblbuildinfosiface.h>

#include <QQuickItem>
#include <QQuickFramebufferObject>
#include <QFlags>
#include <QQueue>
#include <QColor>


class VisualizerItem : public QQuickFramebufferObject
{
    Q_OBJECT

public:
    VisualizerItem(QQuickItem *parent = nullptr);
    ~VisualizerItem();

    QVector3D getTranslation() const
        {return m_viewData.translation;}

    QQuaternion getRotation() const
        {return m_viewData.rotation;}

    float getZoomLevel() const
        {return m_viewData.zoomLevel;}

    const ViewData* getViewData() const
        {return &m_viewData;}

    bool hasEventsAvailable();
    VisualizerEvent takeEvent();
signals:

public slots:
    void resetView();
    void setZoomLevel(float zoom);

    //Relative changes
    void translateFromScreenVector(QVector2D translatedVector);
    void rotateScreen(float angle);
    void rotateCameraFromScreenVector(QVector2D screenVector);
    void applyZoomRatio(float ratio);

    //Events
    void onJobAdded(GCodeJob* job);
    void onJobRemoved(GCodeJob* job);
    void onJobLineStatusChanged(GCodeJob* job, int line, GCodeJobLine::Status lineStatus);
    void onJobPositionChanged(GCodeJob* job, QVector3D position);
    void onJobColorChanged(GCodeJob* job, QColor color);

    void onSelectedJobChanged(GCodeJob* selectedJob);

    void onMachinePositionChanged(QVector3D position);

    void onMachineSpaceChanged(QVector3D machineSpace);
    void onHomingStateChanged(GrblBoard::HomingState homingState);
    void onBuildOptionsChanged(GrblBuildInfosIface::BuildOptions options);

    void onAccessoryStateChanged(GrblStatusIface::AccessoryFlags accessories);

    void onReportImperialUnitsChanged(bool imperialUnitsEnabled);

protected slots:

//    void onWindowChanged(QQuickWindow*);

protected:
    QQuickFramebufferObject::Renderer *createRenderer() const Q_DECL_OVERRIDE;

    void mouseDoubleClickEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
    void wheelEvent(QWheelEvent *e) Q_DECL_OVERRIDE;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) Q_DECL_OVERRIDE;
    void touchEvent(QTouchEvent *event);

    void updateMachineSpace();

    QVector3D getMachineSpaceLimit();
    void showCompleteMachineSpace();

private:
    ViewData m_viewData;

    struct {
        QVector3D limitPosition;
        bool homingPositiveCoord;
        GrblBoard::HomingState homingState;
    } m_machineSpace;

    QQueue<VisualizerEvent> m_eventQueue;

    TouchPointsManager m_touchPointsManager;

    QVector2D m_mouseDownPosition;
};

#endif // VISUALIZERITEM_H
