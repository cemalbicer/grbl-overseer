#include "renderablejobhighlighter.h"
#include "renderablejob.h"

#include <QFont>
#include <QFontMetricsF>
#include <QImage>
#include <QPainter>
#include <QRectF>
#include <QOpenGLTexture>

#define AXES_COUNT          3
#define VERTICES_PER_DIM    20

#define DIM_COLOR           Qt::white
#define DIM_LINES_H         20.0f   // mm
#define DIM_LINES_MARGINS   4.0f    // mm
#define DIM_ARROW_W         1.0f    // mm
#define DIM_ARROW_H         2.0f    // mm
#define TEXT_PIXEL_SIZE     50
#define TEXT_DRAW_HEIGHT    10.0f   // mm
#define TEXT_MARGINS        2.0f    // mm

#define LINES_FIRST_INDEX   0
#define LINES_INDEX_COUNT   48
#define TEXT_FIRST_INDEX    48
#define TEXT_INDEX_COUNT    34

#define MM_PER_INCHES       25.4f


const QVector<GLushort> RenderableJobHighlighter::s_indices = QVector<GLushort>(
{
    // Lines, drawn with DrawMode_Lines
    0,  1,  2,  3,  2,  4,  2,  5,      // X dimension left part
    6,  7,  8,  9,  8,  10, 8,  11,     // X dimension right part
    20, 21, 22, 23, 22, 24, 22, 25,     // Y dimension left part
    26, 27, 28, 29, 28, 30, 28, 31,     // Y dimension right part
    40, 41, 42, 43, 42, 44, 42, 45,     // Z dimension left part
    46, 47, 48, 49, 48, 50, 48, 51,     // Z dimension right part

    // Text, drawn with DrawMode_TriangleStrip
    // (doubled value are for degenerating triangles, and keeping correct odd / even positions)
        12, 13, 14, 15, 15,             // X dimension text front
    16, 16, 17, 18, 19, 19,             // X dimension text back
    32, 32, 33, 34, 35, 35,             // Y dimension text front
    36, 36, 37, 38, 39, 39,             // Y dimension text back
    52, 52, 53, 54, 55, 55,             // Z dimension text front
    56, 56, 57, 58, 59,                 // Z dimension text back
});

RenderableJobHighlighter::RenderableJobHighlighter() :
    RenderableObject(2, 1),
    m_jobToHighlight(nullptr),
    m_usesImperialUnits(false)
{
    //Set rendering configuration
    RenderingConfig & wireFrameRndrConfPtr = getRndrConfRef(0);
    wireFrameRndrConfPtr.drawMode       = RenderingConfig::DrawMode_Lines;
    wireFrameRndrConfPtr.firstVertex    = LINES_FIRST_INDEX;
    wireFrameRndrConfPtr.vertexCount    = LINES_INDEX_COUNT;
    wireFrameRndrConfPtr.useIndices     = true;
    wireFrameRndrConfPtr.colorMode      = RenderingConfig::ColorMode_DirectColor;
    wireFrameRndrConfPtr.lineWidth      = 2.0f;
    wireFrameRndrConfPtr.mMatrixIndex   = 0;
    wireFrameRndrConfPtr.colorAFactor   = QVector4D(0.0f,0.0f,0.0f,0.0f);   // Don't keep color
    wireFrameRndrConfPtr.colorBFactor   = QVector4D(1.0f,1.0f,1.0f,1.0f);   // Set color here

    RenderingConfig & textRndrConfPtr = getRndrConfRef(1);
    textRndrConfPtr.drawMode       = RenderingConfig::DrawMode_TriangleStrip;
    textRndrConfPtr.firstVertex    = TEXT_FIRST_INDEX;
    textRndrConfPtr.vertexCount    = TEXT_INDEX_COUNT;
    textRndrConfPtr.useIndices     = true;
    textRndrConfPtr.colorMode      = RenderingConfig::ColorMode_TextureCoord;
    textRndrConfPtr.lineWidth      = 2.0f;
    textRndrConfPtr.mMatrixIndex   = 0;
    textRndrConfPtr.colorAFactor   = QVector4D(1.0f,1.0f,1.0f,1.0f);    // Only keep alpha
    textRndrConfPtr.colorBFactor   = QVector4D(0.0f,0.0f,0.0f,0.0f);    // Set color here

    createIndexBuffer(s_indices.size(), s_indices.data());
}

void RenderableJobHighlighter::setHighlightedJob(RenderableJob *job)
{
    m_jobToHighlight = job;
    regenerate();
}

RenderableJob *RenderableJobHighlighter::getSelectedJob() const
{
    return m_jobToHighlight;
}

void RenderableJobHighlighter::updateOffset()
{
    getMMatrixRef(0) = generateMMatrix(m_jobToHighlight->getOffset());
}

void RenderableJobHighlighter::setUseImperialUnits(bool use)
{
    m_usesImperialUnits = use;
    regenerate();
}


QString RenderableJobHighlighter::getLengthString(float length)
{
    if(m_usesImperialUnits) length /= MM_PER_INCHES;

    return QString::number(length,'f',3)
            + (m_usesImperialUnits ? " in" : " mm");
}

void RenderableJobHighlighter::regenerate()
{
    if(m_jobToHighlight)
    {
        QVector3D texturesAspectRatio;
        rebuildTexture(&texturesAspectRatio);
        rebuildGeometry(texturesAspectRatio);
        updateOffset();
    }
}

void RenderableJobHighlighter::rebuildTexture(QVector3D *textsAspectRatio)
{
    QFont textFont;
    textFont.setPixelSize(TEXT_PIXEL_SIZE);

    //Generate strings for each length
    QString lengthStrings[AXES_COUNT] = {
        getLengthString(m_jobToHighlight->getLimitMax().x() -
                        m_jobToHighlight->getLimitMin().x()),
        getLengthString(m_jobToHighlight->getLimitMax().y() -
                        m_jobToHighlight->getLimitMin().y()),
        getLengthString(m_jobToHighlight->getLimitMax().z() -
                        m_jobToHighlight->getLimitMin().z())
    };

    //Generate bounding rectangle for each strings
    QFontMetricsF fontMetrics(textFont);
    QRectF maxStringBoundRect;
    for(quint8 i = 0 ; i < AXES_COUNT ; i++)
    {
        QRectF boundingRect = fontMetrics.boundingRect(lengthStrings[i]);
        maxStringBoundRect |= boundingRect;
        (*textsAspectRatio)[i] = boundingRect.width();
    }
    (*textsAspectRatio)/= maxStringBoundRect.height();

    //Construct an image that can fit all 3 texts
    QImage textureImage(maxStringBoundRect.width(),
                        maxStringBoundRect.height() * AXES_COUNT,
                        QImage::Format_ARGB32);
    textureImage.fill(0x00000000); //Transparent image

    //Draw 3 dimension text
    QPainter texturePainter(&textureImage);
    texturePainter.setFont(textFont);
    texturePainter.setPen(DIM_COLOR);
    maxStringBoundRect.moveLeft(0);
    for(quint8 i = 0 ; i < AXES_COUNT ; i++)
    {
        maxStringBoundRect.moveTop(maxStringBoundRect.height() * i);
        texturePainter.drawText(maxStringBoundRect,Qt::AlignCenter, lengthStrings[i]);
    }

    //if(textureImage.save("textimage.png"))   qDebug() << "Image saved";   //DEBUG

    //Now use the image as OpenGL texture
    QOpenGLTexture *texture = new QOpenGLTexture(textureImage,QOpenGLTexture::DontGenerateMipMaps);
    texture->setMagnificationFilter(QOpenGLTexture::Linear);
    texture->setMinificationFilter(QOpenGLTexture::Linear);
    texture->setWrapMode(QOpenGLTexture::ClampToEdge);

    setTexture(texture);
}

void RenderableJobHighlighter::rebuildGeometry(QVector3D textsAspectRatio)
{
    //We will use thoses values a lot, better keep then in a handy way
    const float xn = m_jobToHighlight->getLimitMin().x();
    const float yn = m_jobToHighlight->getLimitMin().y();
    const float zn = m_jobToHighlight->getLimitMin().z();
    const float xp = m_jobToHighlight->getLimitMax().x();
    const float yp = m_jobToHighlight->getLimitMax().y();
    const float zp = m_jobToHighlight->getLimitMax().z();

    QVector<VertexData> verticesVector;
    verticesVector.reserve(VERTICES_PER_DIM * AXES_COUNT);

    //Build vertices for X dimension
    verticesVector.append(buildDimensionVertices(xn, xp, yn, zn,
                                                 textsAspectRatio,
                                                 Axis_X));
    //Build vertices for Y dimension
    verticesVector.append(buildDimensionVertices(yp, yn, xp, zn,
                                                 textsAspectRatio,
                                                 Axis_Y));

    //Build vertices for Z dimension
    verticesVector.append(buildDimensionVertices(zn, zp, xn, yn,
                                                 textsAspectRatio,
                                                 Axis_Z));

    createVertexBuffer(verticesVector.size(),verticesVector.data());
}

QVector<RenderableObject::VertexData> RenderableJobHighlighter::buildDimensionVertices(float leftCoord, float rightCoord,
                                                                    float baseCoord, float thirdAxisPos,
                                                                    QVector3D textsAspectRatio,
                                                                    Axis_Enum axis)
{
    //first compute label height /width
    float labelHeight = TEXT_DRAW_HEIGHT;
    float labelWidth = textsAspectRatio[axis] * TEXT_DRAW_HEIGHT;

    float dir = (leftCoord > rightCoord) ? -1.0f : 1.0f;

    float totalWidth = (rightCoord - leftCoord);
    float midwayCoord = (leftCoord + rightCoord) / 2.0f;

    float leftCoord1    = leftCoord + dir * DIM_ARROW_H;
    float rightCoord1   = rightCoord - dir * DIM_ARROW_H;

    float yCoordTop         = baseCoord - dir * DIM_LINES_MARGINS ;
    float yCoordMiddleLine  = yCoordTop - dir * DIM_LINES_H ;
    float yCoordArrowBottom = yCoordMiddleLine - dir * DIM_ARROW_W;
    float yCoordArrowTop    = yCoordMiddleLine + dir * DIM_ARROW_W;

    //Determine whether or not label can fit between arrows
    float textWidth = (qAbs(totalWidth) - 2.0f * (DIM_ARROW_H + TEXT_MARGINS));
    bool canLabelFitInside =  textWidth > labelWidth;

    //Those are used by label vertices
    float labelLeftCoord =  midwayCoord - dir * labelWidth / 2.0f;
    float labelRightCoord = midwayCoord + dir * labelWidth / 2.0f;
    float labelTopCoord     = yCoordMiddleLine - dir * labelHeight / 2.0f;
    float labelBottomCoord  = yCoordMiddleLine + dir * labelHeight / 2.0f;

    //If text does not fit between arrows, place it outside
    if(!canLabelFitInside)
    {
        labelTopCoord       -= dir * labelHeight;
        labelBottomCoord    -= dir * labelHeight;
    }

    //Those are used by lines vertices
    //if text does not fit between arrow, connect arows together
    float lineLeftCoord = canLabelFitInside
            ? (labelLeftCoord - dir * TEXT_MARGINS)
            : midwayCoord;
    float lineRightCoord = canLabelFitInside
            ? (labelRightCoord + dir * TEXT_MARGINS)
            : midwayCoord;

    //Determine texture boundaries
    float textureAspectRatio = 0.0f;    //texture aspect ratio is the largest text aspect ratio
    for(quint8 i = 0 ; i < AXES_COUNT ; i++)
    {
        if(textsAspectRatio[i] > textureAspectRatio)
            textureAspectRatio = textsAspectRatio[i];
    }
    float textHorizBoundRatio = textsAspectRatio[axis] / textureAspectRatio;

    float tx0 = 0.5f - 0.5f * textHorizBoundRatio;
    float tx1 = 0.5f + 0.5f * textHorizBoundRatio;
    float ty0 = axis * 0.33f;
    float ty1 = ty0 + 0.33f;


    return QVector<VertexData>(
    {
        //Left half
        { vec3Axis(leftCoord         , yCoordTop        , thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(leftCoord         , yCoordArrowBottom, thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(leftCoord         , yCoordMiddleLine , thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(leftCoord1        , yCoordArrowBottom, thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(leftCoord1        , yCoordArrowTop   , thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(lineLeftCoord     , yCoordMiddleLine , thirdAxisPos, axis), QVector4D()        },

        //right half
        { vec3Axis(rightCoord        , yCoordTop        , thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(rightCoord        , yCoordArrowBottom, thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(rightCoord        , yCoordMiddleLine , thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(rightCoord1       , yCoordArrowBottom, thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(rightCoord1       , yCoordArrowTop   , thirdAxisPos, axis), QVector4D()        },
        { vec3Axis(lineRightCoord    , yCoordMiddleLine , thirdAxisPos, axis), QVector4D()        },

        //label front
        { vec3Axis(labelLeftCoord    , labelTopCoord    , thirdAxisPos, axis), QVector4D(tx0, ty1, 0.0f, 0.0f)},
        { vec3Axis(labelLeftCoord    , labelBottomCoord , thirdAxisPos, axis), QVector4D(tx0, ty0, 0.0f, 0.0f)},
        { vec3Axis(labelRightCoord   , labelTopCoord    , thirdAxisPos, axis), QVector4D(tx1, ty1, 0.0f, 0.0f)},
        { vec3Axis(labelRightCoord   , labelBottomCoord , thirdAxisPos, axis), QVector4D(tx1, ty0, 0.0f, 0.0f)},

        //label back
        { vec3Axis(labelLeftCoord    , labelTopCoord    , thirdAxisPos, axis), QVector4D(tx1, ty1, 0.0f, 0.0f)},
        { vec3Axis(labelRightCoord   , labelTopCoord    , thirdAxisPos, axis), QVector4D(tx0, ty1, 0.0f, 0.0f)},
        { vec3Axis(labelLeftCoord    , labelBottomCoord , thirdAxisPos, axis), QVector4D(tx1, ty0, 0.0f, 0.0f)},
        { vec3Axis(labelRightCoord   , labelBottomCoord , thirdAxisPos, axis), QVector4D(tx0, ty0, 0.0f, 0.0f)}
    });
}

QVector3D RenderableJobHighlighter::vec3Axis(float x, float y, float z, RenderableJobHighlighter::Axis_Enum axis)
{
    switch(axis)
    {
    case Axis_X:
        return QVector3D(x, y, z);
    case Axis_Y:
        return QVector3D(y, x, z);
    case Axis_Z:
        return QVector3D(y, z ,x);
    default:
        return QVector3D();
    }
}

void RenderableJobHighlighter::render(QOpenGLShaderProgram *program)
{
    if(m_jobToHighlight)
    {
        m_jobToHighlight->render(program);
        m_jobToHighlight->renderHalo(program);

        RenderableObject::render(program);
    }
}
