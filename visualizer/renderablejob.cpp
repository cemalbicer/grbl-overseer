#include "renderablejob.h"
#include "../gcode/gcodejob.h"

//Static init of s_lineStatusToVertexStatusMap
const QMap<GCodeJobLine::Status,RenderableJob::VertexStatusEnum> RenderableJob::s_lineStatusToVertexStatusMap =
        QMap<GCodeJobLine::Status,RenderableJob::VertexStatusEnum>(
        {
            {GCodeJobLine::Line_Ready   , RenderableJob::VertexStatus_Ready     },
            {GCodeJobLine::Line_Queued  , RenderableJob::VertexStatus_Queued    },
            {GCodeJobLine::Line_Sent    , RenderableJob::VertexStatus_Sent      },
            {GCodeJobLine::Line_Accepted, RenderableJob::VertexStatus_Accepted  },
            {GCodeJobLine::Line_Refused , RenderableJob::VertexStatus_Refused   }
        });

const QVector<QVector4D> RenderableJob::s_colorPalette =
        QVector<QVector4D> (
        {
            QVector4D(1.0f, 1.0f, 1.0f, 1.0f),  // Work idle
            QVector4D(0.5f, 0.5f, 0.5f, 1.0f),  // Work queued
            QVector4D(0.5f, 0.5f, 0.5f, 1.0f),  // Work sent
            QVector4D(1.0f, 1.0f, 1.0f, 1.0f),  // Work accepted
            QVector4D(1.0f, 1.0f, 1.0f, 1.0f),  // Work refused

            QVector4D(1.0f, 1.0f, 1.0f, 0.2f),  // Move idle
            QVector4D(0.5f, 0.5f, 0.5f, 0.2f),  // Move queued
            QVector4D(0.5f, 0.5f, 0.5f, 0.2f),  // Move sent
            QVector4D(1.0f, 1.0f, 1.0f, 0.2f),  // Move accepted
            QVector4D(1.0f, 1.0f, 1.0f, 0.2f),  // Move refused
        });

RenderableJob::RenderableJob(const GCodeJob *job) :
    RenderableObject(2, 1),
    m_limitMin(job->getLimitMin()),
    m_limitMax(job->getLimitMax())
{
    const quint32 jobLineCount = job->getLineCount();
    GLint vertexCount = 0;

    //Build the verticesInfo vector while counting vertices
    m_verticesInfos.reserve(jobLineCount); //Optimisation : final size already known, avoid reallocations
    for(quint32 i = 0 ; i < jobLineCount ; i++)
    {
        const GCodeJobLine* currentLine = job->getLine(i+1);

        GLsizei currentLineVerticesCount = currentLine->getPath().length();

        m_verticesInfos.append({vertexCount,
                                currentLineVerticesCount,
                                currentLine->getIsWork() ? VertexType_Work : VertexType_Move});

        vertexCount += currentLineVerticesCount;
    }

    //Create VBO
    createVertexBuffer(vertexCount);

    //Setup main rendering config
    RenderingConfig & mainRndrConfPtr = getRndrConfRef(0);
    mainRndrConfPtr.drawMode        = RenderingConfig::DrawMode_LineStrip;
    mainRndrConfPtr.vertexCount     = vertexCount;
    mainRndrConfPtr.useIndices      = false;
    mainRndrConfPtr.colorMode       = RenderingConfig::ColorMode_DirectColor;
    mainRndrConfPtr.lineWidth       = 2.0f;
    mainRndrConfPtr.mMatrixIndex    = 0;
    mainRndrConfPtr.colorAFactor    = colorToVector4D(job->getColor());
    mainRndrConfPtr.colorBFactor    = QVector4D(0.0f,0.0f,0.0f,0.0f);

    //Setup halo rendering config
    RenderingConfig & haloRndrConfPtr = getRndrConfRef(1);
    haloRndrConfPtr.drawMode        = RenderingConfig::DrawMode_LineStrip;
    haloRndrConfPtr.vertexCount     = vertexCount;
    haloRndrConfPtr.useIndices      = false;
    haloRndrConfPtr.colorMode       = RenderingConfig::ColorMode_DirectColor;
    haloRndrConfPtr.lineWidth       = 4.0f;
    haloRndrConfPtr.mMatrixIndex    = 0;
    haloRndrConfPtr.colorAFactor    = QVector4D(0.0f,0.0f,0.0f,0.0f);
    haloRndrConfPtr.colorBFactor    = QVector4D(1.0f,1.0f,1.0f,1.0f);

    //Now fill the VBO with data
    for(quint32 i = 0 ; i < jobLineCount ; i++)
    {
        const GCodeJobLine* currentLine = job->getLine(i+1);

        //Copy each vertex to VBO
        quint32 firstVertexIndex = m_verticesInfos.at(i).firstVertexIndex;
        VertexTypeEnum lineType = m_verticesInfos.at(i).vertexType;

        QVector<QVector3D> currLinePath = currentLine->getPath();

        //Color can be set now, it won't change for the entire line
        VertexData vertexData = {
            .position = QVector3D(),
            .colorInfo = getLineColor(lineType, currentLine->getStatus())
        };

        //Set position of each individual vertices
        for(quint32 vIndex = 0 ; vIndex < (quint32)currLinePath.size() ; vIndex++)
        {
            vertexData.position = currLinePath.at(vIndex);

            quint32 firstIndexToWrite = (firstVertexIndex + vIndex) * sizeof(VertexData);

            writeVertexBuffer(firstIndexToWrite, sizeof(VertexData), &vertexData);
        }
    }

    setOffset(job->getOrigin());
}

void RenderableJob::updateLineStatus(int fromLine, int toLine, GCodeJobLine::Status lineStatus)
{
    Q_ASSERT(fromLine > 0 && fromLine <= toLine && toLine <= m_verticesInfos.size());

    for(int i = fromLine ; i <= toLine ; i++)
    {
        const LineVerticesStruct& lineVerticesInfo = m_verticesInfos.at(i-1);

        QVector4D segmentColor = getLineColor(lineVerticesInfo.vertexType, lineStatus);

        for(GLint vIndex = 0 ; vIndex < lineVerticesInfo.vertexCount ; vIndex++)
        {
            GLint bufferIndexToWrite =
                    (lineVerticesInfo.firstVertexIndex + vIndex) * sizeof(VertexData);

            writeVertexBuffer(bufferIndexToWrite + offsetof(VertexData, colorInfo),
                              sizeof(QVector4D),
                              &segmentColor);
        }
    }
}

QVector4D RenderableJob::getLineColor(RenderableJob::VertexTypeEnum lineType, GCodeJobLine::Status lineStatus)
{
    VertexStatusEnum vertexStatus = s_lineStatusToVertexStatusMap.value(lineStatus,VertexStatus_Queued);
    return s_colorPalette.at(lineType + vertexStatus);
}

void RenderableJob::setColor(QColor color)
{
    RenderingConfig & mainRndrConfPtr = getRndrConfRef(0);
    mainRndrConfPtr.colorAFactor = colorToVector4D(color);
}

void RenderableJob::setOffset(QVector3D offset)
{
    m_offset = offset;
    getMMatrixRef(0) = generateMMatrix(offset);
}

void RenderableJob::render(QOpenGLShaderProgram *program)
{
    performRendering(program,0);
}

void RenderableJob::renderHalo(QOpenGLShaderProgram *program)
{
    performRendering(program, 1);
}

QVector3D RenderableJob::getLimitMin() const
{
    return m_limitMin;
}

QVector3D RenderableJob::getLimitMax() const
{
    return m_limitMax;
}

QVector3D RenderableJob::getOffset() const
{
    return m_offset;
}
