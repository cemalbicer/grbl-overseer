#ifndef VISUALISERITEMRENDERER_H
#define VISUALISERITEMRENDERER_H

#include "visualizerevent.h"

#include <QObject>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QQuickWindow>
#include <QQuickFramebufferObject>
#include <QMatrix4x4>

class GCodeJob;
class RenderableAxes;
class RenderableJob;
class RenderableSpindle;
class RenderableMachineSpace;
class RenderableJobHighlighter;
class RenderableCenterIndicator;
class RenderableRotationIndicator;

struct ViewData {
    QVector3D translation;
    QQuaternion rotation;
    float zoomLevel;
    QVector2D viewPortSize;

    bool operator!=(const ViewData &other) const
    {
        return translation != other.translation ||
                rotation != other.rotation ||
                zoomLevel != other.zoomLevel ||
                viewPortSize != other.viewPortSize;
    }
};


class VisualizerItemRenderer : public QObject, public QQuickFramebufferObject::Renderer, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    VisualizerItemRenderer();
    ~VisualizerItemRenderer();

public slots:

protected:
    QOpenGLFramebufferObject *createFramebufferObject(const QSize &size);
    void render();
    void synchronize(QQuickFramebufferObject *item);

private:

    void processJobEvent(VisualizerEvent &event);

    void render3dElements();
    void render2dElements();

    void processViewData(const ViewData* newViewData);

    ViewData m_lastViewData;

    QMatrix4x4 m_2dVpMatrix;
    QMatrix4x4 m_3dVpMatrix;
    QOpenGLShaderProgram *m_program;
    QQuickWindow *m_window;

//    RenderableAxes *m_axes;

    RenderableSpindle *m_drill;

    QMap<const GCodeJob*,RenderableJob*> m_jobs;

    RenderableJobHighlighter* m_jobHighlighter;

    RenderableMachineSpace* m_machineSpace;

    RenderableCenterIndicator* m_centerIndicator;

    RenderableRotationIndicator* m_rotationIndicator;
};

#endif // VISUALISERITEMRENDERER_H
