#include "renderablespindle.h"

#include <grbl/grblstatuslogic.h> //!<for STATUS_DEFAULT_REQUEST_INTERVAL

#include <QtMath>
#include <QColor>
#include <QOpenGLTexture>

#define VERTICES_PER_SUBDIVISION    4
#define INDICES_PER_SUBDIVION       8

#define FULL_ROTATION_VELOCITY  (1.5f)  // Rev/sec

// SMOOTH TIME is a bit larger than GRBLBOARD status refresh interval
// to ensure smooth animation despite response jitter
#define DRILL_SMOOTH_MOVE_TIME (STATUS_DEFAULT_REQUEST_INTERVAL * 1.05f)

RenderableSpindle::RenderableSpindle(float height, float radius, quint8 detailLevel) :
    RenderableObject(2, 1),
    m_rotAngle(0.0f),
    m_rotVelocity(0.0f)
{
    const quint16 subdivCount =  qPow(2.0f, detailLevel + 2);

    //Build vertex data buffer
    QVector<RenderableObject::VertexData> vertexArray;
    vertexArray.reserve((subdivCount + 1) * VERTICES_PER_SUBDIVISION);
    for (quint16 i = 0 ; i <= subdivCount ; i++)
    {
        float circlePosition = (float)i / (float)subdivCount;
        vertexArray.append(getSubdivVertices(circlePosition, height, radius));
    }
    createVertexBuffer(vertexArray.size(), vertexArray.data());

    //Build index buffer
    QVector<GLushort> indexArray;
    indexArray.reserve(subdivCount * INDICES_PER_SUBDIVION);
    for (quint16 i = 0 ; i < subdivCount ; i++)
    {
        indexArray.append(getSubdivIndices(i));
    }

    //Those are for the internal vertical line
    //This line makes it easier to see where the spindle points when seen
    // from the top
    indexArray.append({(GLushort) 0, ((GLushort)VERTICES_PER_SUBDIVISION - 1)});

    createIndexBuffer(indexArray.size(), indexArray.data());

    //Set texture
    QImage textureImage(4,4,QImage::Format_ARGB32);

    textureImage.setPixel(0,0,0xa0000000);
    textureImage.setPixel(0,1,0xa0000000);
    textureImage.setPixel(0,2,0xa0ffffff);
    textureImage.setPixel(0,3,0xa0000000);

    textureImage.setPixel(1,0,0xa0ffffff);
    textureImage.setPixel(1,1,0xa0ffffff);
    textureImage.setPixel(1,2,0xa0000000);
    textureImage.setPixel(1,3,0xa0ffffff);

    textureImage.setPixel(2,0,0xa0000000);
    textureImage.setPixel(2,1,0xa0000000);
    textureImage.setPixel(2,2,0xa0ffffff);
    textureImage.setPixel(2,3,0xa0000000);

    textureImage.setPixel(3,0,0xa0ffffff);
    textureImage.setPixel(3,1,0xa0ffffff);
    textureImage.setPixel(3,2,0xa0000000);
    textureImage.setPixel(3,3,0xa0ffffff);

    QOpenGLTexture *texture = new QOpenGLTexture(textureImage,QOpenGLTexture::DontGenerateMipMaps);
    texture->setMagnificationFilter(QOpenGLTexture::Nearest);
    texture->setMinificationFilter(QOpenGLTexture::Nearest);
    texture->setWrapMode(QOpenGLTexture::Repeat);

    setTexture(texture);

    //Set rendering configuration (textured)
    RenderingConfig & mainRndrConfPtr = getRndrConfRef(0);
    mainRndrConfPtr.drawMode        = RenderingConfig::DrawMode_TriangleStrip;
    mainRndrConfPtr.firstVertex     = 0;
    mainRndrConfPtr.vertexCount     = subdivCount * INDICES_PER_SUBDIVION;
    mainRndrConfPtr.useIndices      = true;
    mainRndrConfPtr.colorMode       = RenderingConfig::ColorMode_TextureCoord;
    mainRndrConfPtr.lineWidth       = 2.0f;
    mainRndrConfPtr.mMatrixIndex    = 0;
    mainRndrConfPtr.colorAFactor    = QVector4D(1.0f,1.0f,1.0f,1.0f);
    mainRndrConfPtr.colorBFactor    = QVector4D(0.0f,0.0f,0.0f,0.0f);

    //Set rendering configuration (inside line)
    RenderingConfig & intLineRndrConfPtr = getRndrConfRef(1);
    intLineRndrConfPtr.drawMode         = RenderingConfig::DrawMode_Lines;
    intLineRndrConfPtr.firstVertex      = mainRndrConfPtr.vertexCount;
    intLineRndrConfPtr.vertexCount      = 2;
    intLineRndrConfPtr.useIndices       = true;
    intLineRndrConfPtr.colorMode        = RenderingConfig::ColorMode_DirectColor;
    intLineRndrConfPtr.lineWidth        = 2.0f;
    intLineRndrConfPtr.mMatrixIndex     = 0;
    intLineRndrConfPtr.colorAFactor     = QVector4D(0.0f,0.0f,0.0f,0.0f);
    intLineRndrConfPtr.colorBFactor     = QVector4D(1.0f,1.0f,1.0f,1.0f);
}

void RenderableSpindle::setPosition(QVector3D position)
{
    m_timeSincePosReceived.start();

    m_prevPos = m_currPos;
    m_destPos = position;

    m_velocity = (m_destPos - m_prevPos) / DRILL_SMOOTH_MOVE_TIME;
}

void RenderableSpindle::setRotation(float velocity)
{
    //Clamp rotationVelocity
    velocity = qBound(-1.0f, velocity, 1.0f);

    //If not previously rotating, init rotation time
    if(velocity != 0.0f && m_rotVelocity == 0.0f)
    {
        m_timeSinceLastRotation.start();
    }

    m_rotVelocity = velocity;
}

bool RenderableSpindle::update()
{
    bool moving = (m_currPos != m_destPos);
    bool rotating = (m_rotVelocity != 0.0f);

    // Position
    if(moving)
    {
        int elapsedMsSinceLastPosReceived = m_timeSincePosReceived.elapsed();

        moving &= (elapsedMsSinceLastPosReceived < DRILL_SMOOTH_MOVE_TIME);

        m_currPos = moving
                ? m_prevPos + m_velocity * elapsedMsSinceLastPosReceived
                : m_destPos;
    }

    // Rotation
    if(rotating)
    {
        int elapsedMsSinceLastRotation = m_timeSinceLastRotation.restart();
        float angleToAdd = elapsedMsSinceLastRotation * (FULL_ROTATION_VELOCITY * 360.0f / 1000.0f);
        m_rotAngle += m_rotVelocity > 0 ? -angleToAdd : angleToAdd;

        //Clamp angle between -180.0° and 180.0°
        while(m_rotAngle > +180.0f) m_rotAngle -= 360.0f;
        while(m_rotAngle < -180.0f) m_rotAngle += 360.0f;
    }

    getMMatrixRef(0) = generateMMatrix(m_currPos, QQuaternion::fromEulerAngles(0.0f,0.0f,m_rotAngle));

    return moving | rotating;
}


QVector<RenderableObject::VertexData> RenderableSpindle::getSubdivVertices(float circlePos, float height, float radius)
{
    float angle = circlePos * 2.0f * M_PI;

    QVector2D edgeCoord = QVector2D(qCos(angle), qSin(angle)) * radius;
    QVector2D centerCoord = QVector2D(0.0f,0.0f);

    QVector<RenderableObject::VertexData> subdivVertices;
    subdivVertices.reserve(VERTICES_PER_SUBDIVISION);

    subdivVertices.append({QVector3D(centerCoord    , 0.0f) ,
                           QVector4D(circlePos, 0.00f, 0.0f, 0.0f)});

    subdivVertices.append({QVector3D(edgeCoord      , height * 0.5f) ,
                           QVector4D(circlePos, 0.50f, 0.0f, 0.0f)});

    subdivVertices.append({QVector3D(edgeCoord      , height),
                           QVector4D(circlePos, 0.75f, 0.0f, 0.0f)});

    subdivVertices.append({QVector3D(centerCoord    , height),
                           QVector4D(circlePos, 1.00f, 0.0f, 0.0f)});

    return subdivVertices;
}

QVector<GLushort> RenderableSpindle::getSubdivIndices(quint16 currSubdivIndex)
{
    GLushort index = currSubdivIndex * VERTICES_PER_SUBDIVISION;
    return QVector<GLushort>(
    {
        GLushort(index),                                    //    6,7    //
        GLushort(index),                                    //    / \    //
        GLushort(index + VERTICES_PER_SUBDIVISION + 1),     //   5 - 4   //
        GLushort(index + 1),                                //   |   |   //
        GLushort(index + VERTICES_PER_SUBDIVISION + 2),     //   3 - 2   //
        GLushort(index + 2),                                //    \ /    //
        GLushort(index + 3),                                //    0,1    //
        GLushort(index + 3)
    });
}
