import QtQuick 2.4

Item {
    //: The Ok button in generic dialogs
    //% "Ok"
    property string dialogButtonOk: qsTrId("dialog-button-ok")

    //: The Yes button in generic dialogs
    //% "Yes"
    property string dialogButtonYes: qsTrId("dialog-button-yes")

    //: The No button in generic dialogs
    //% "No"
    property string dialogButtonNo: qsTrId("dialog-button-no")

    //: The Abort button in generic dialogs
    //% "Abort"
    property string dialogButtonAbort: qsTrId("dialog-button-abort")

    //: The placeholder in GRBL config value field
    //% "Value"
    property string grblSettingsValuePlaceholder: qsTrId("grbl-settings-value-placeholder")

    //: The label for "true" in GRBL config bool field
    //% "Yes"
    property string grblSettingsValueBoolYes: qsTrId("grbl-settings-value-bool-yes")

    //: The label for "false" in GRBL config bool field
    //% "No"
    property string grblSettingsValueBoolNo: qsTrId("grbl-settings-value-bool-no")

    //: The line count in a task
    //% "%1 lines"
    property string labelLinesCount: qsTrId("label-lines-count")

    //: The label of the "Set origin here" job button
    //% "Set origin here"
    property string jobButtonSetOriginHere: qsTrId("job-button-set-origin-here")

    //: The label of the "Change color" job button
    //% "Change color"
    property string jobButtonRandomiseColor: qsTrId("job-button-randomise-color")

    //: The label of the "Delete job" job button
    //% "Delete job"
    property string jobButtonDelete: qsTrId("job-button-delete")

    //: The label of the X axis
    //% "X"
    property string axisLabelX: qsTrId("axis-label-x")

    //: The label of the Y axis
    //% "Y"
    property string axisLabelY: qsTrId("axis-label-y")

    //: The label of the Z axis
    //% "Z"
    property string axisLabelZ: qsTrId("axis-label-z")

    //: The label of the X+ axis
    //% "X+"
    property string axisLabelXPositive: qsTrId("axis-label-x-positive")

    //: The label of the X- axis
    //% "X-"
    property string axisLabelXNegative: qsTrId("axis-label-x-negative")

    //: The label of the Y+ axis
    //% "Y+"
    property string axisLabelYPositive: qsTrId("axis-label-y-positive")

    //: The label of the Y- axis
    //% "Y-"
    property string axisLabelYNegative: qsTrId("axis-label-y-negative")

    //: The label of the Z+ axis
    //% "Z+"
    property string axisLabelZPositive: qsTrId("axis-label-z-positive")

    //: The label of the Z- axis
    //% "Z-"
    property string axisLabelZNegative: qsTrId("axis-label-z-negative")

    //: The label of the jog control group title
    //% "Jog"
    property string labelJogGroup: qsTrId("label-jog-group")

    //: The manual control group title
    //% "Manual Commands"
    property string labelManualControlGroup: qsTrId("label-manual-control-group")

    //% "Run homing"
    property string buttonHoming: qsTrId("button-homing")

    //% "Kill alarm"
    property string buttonAlarmReset: qsTrId("button-alarm-reset")

    //% "Overrides"
    property string labelOverridesGroup: qsTrId("label-overrides-group")

    //% "Feed motions"
    property string labeFeedRatel: qsTrId("label-feed-rate")

    //% "Rapid motions"
    property string labelMoveRate: qsTrId("label-move-rate")

    //% "Spindle RPM"
    property string labelSpinRate: qsTrId("label-spin-rate")

    //% "Load more G-Code jobs"
    property string buttonLoadMoreJobs: qsTrId("button-load-more-jobs")

    //% "Load G-Code jobs"
    property string buttonLoadJobs: qsTrId("button-load-job")



    //% "Plan Buf : Err"
    property string labelMonitorPlanBufferError: qsTrId("label-monitor-plan-buffer-error")

    //% "Plan Buf : "
    property string labelMonitorPlanBuffer: qsTrId("label-monitor-plan-buffer")

    //% "Char Buf : Err"
    property string labelMonitorCharBufferError: qsTrId("label-monitor-char-buffer-error")

    //% "Char Buf : "
    property string labelMonitorCharBuffer: qsTrId("label-monitor-char-buffer")

    //% "Type manual commands here"
    property string inputFieldManualCommand: qsTrId("input-field-manual-command")

    //% "Serial Link"
    property string labelSerialLinkGroup: qsTrId("label-serial-link-group")

    //% "Disconnect"
    property string buttonSerialLinkDisconnect: qsTrId("button-serial-link-disconnect")

    //% "Connect"
    property string buttonSerialLinkConnect: qsTrId("button-serial-link-connect")

    //% "GRBL Configuration"
    property string labelGrblConfGroup: qsTrId("label-grbl-conf-group")

    //% "Machine\nconfiguration\nnot available"
    property string labelSettingsMachineConfigNotAvailable: qsTrId("label-settings-machine-config-not-available")

    //% "Work."
    property string labelCoordinatesWork: qsTrId("label-coordinates-work")

    //% "Mach."
    property string labelCoordinatesMachine: qsTrId("label-coordinates-machine")



    //% "Grbl State"
    property string labelStatusTitle: qsTrId("label-status-title")

    //% "Idle"
    property string labelGrblStatusIdle: qsTrId("label-grbl-status-idle");

    //% "Running"
    property string labelGrblStatusRunning: qsTrId("label-grbl-status-running");

    //% "Paused"
    property string labelGrblStatusPaused: qsTrId("label-grbl-status-paused ");

    //% "Jogging"
    property string labelGrblStatusJogging: qsTrId("label-grbl-status-jogging");

    //% "Door"
    property string labelGrblStatusDoor: qsTrId("label-grbl-status-door");

    //% "Homing"
    property string labelGrblStatusHoming: qsTrId("label-grbl-status-homing");

    //% "Alarm"
    property string labelGrblStatusAlarm: qsTrId("label-grbl-status-alarm");

    //% "Simulation"
    property string labelGrblStatusSimulation: qsTrId("label-grbl-status-simulation");

    //% "Sleeping"
    property string labelGrblStatusSleeping: qsTrId("label-grbl-status-sleeping")

    //% "Status unknown"
    property string labelGrblStatusUnknown: qsTrId("label-grbl-status-unknown");

    //% "Not connected"
    property string labelGrblStatusDisconnected: qsTrId("label-grbl-status-disconnected");


    //% "Jobs"
    property string tabJobs: qsTrId("tab-jobs")

    //% "Controls"
    property string tabControls: qsTrId("tab-controls")

    //% "Monitor"
    property string tabMonitor: qsTrId("tab-monitor")

    //% "Settings"
    property string tabSettings: qsTrId("tab-settings")


    //% "Go\n(No Sim.)"
    property string buttonStartNoSim: qsTrId("button-start-no-sim")

    //% "Go"
    property string buttonStart: qsTrId("button-start")

    //% "Resume"
    property string buttonResume: qsTrId("button-resume")

    //% "Pause"
    property string buttonPause: qsTrId("button-pause")

    //% "Reset"
    property string buttonReset: qsTrId("button-reset")

    //% "Error opening job"
    property string dialogOpenJobErrorTitle: qsTrId("dialog-open-job-error-title")

    //% "An error was encountered while trying to open a job"
    property string dialogOpenJobErrorText: qsTrId("dialog-open-job-error-text")

    //% "Error message :"
    property string dialogOpenJobErrorInfo: qsTrId("dialog-open-job-error-info")

    //% "All jobs completed !"
    property string dialogAllJobsCompleteTitle: qsTrId("dialog-all-jobs-complete-title")

    //% "Simulation completed with errors"
    property string dialogJobSimErrorTitle: qsTrId("dialog-job-sim-error-title")

    //% "Start production anyway ?"
    property string dialogJobSimErrorText: qsTrId("dialog-job-sim-error-text")

    //% "Errors :"
    property string dialogJobSimErrorInfo: qsTrId("dialog-job-sim-error-info")

    //% "Simulation completed without errors"
    property string dialogJobSimCompleteTitle: qsTrId("dialog-job-sim-complete-title")

    //% "Start production ?"
    property string dialogJobSimCompleteText: qsTrId("dialog-job-sim-complete-text")

    //% "Command Error"
    property string dialogErrorDuringProductionTitle: qsTrId("dialog-error-during-production-title")

    //% "Grbl just returned an error. Continue despite error ?"
    property string dialogErrorDuringProductionText: qsTrId("dialog-error-during-production-text")

    //% "Error message :"
    property string dialogErrorDuringProductionInfo: qsTrId("dialog-error-during-production-info")

    //% "Alarm"
    property string dialogAlarmTitle: qsTrId("dialog-alarm-title")

    //% "Grbl is in Alarm state. Use Kill Alarm or Run homing commands in Controls tab to return to idle mode"
    property string dialogAlarmText: qsTrId("dialog-alarm-text")

    //% "Alarm message : "
    property string dialogAlarmInfo: qsTrId("dialog-alarm-info")

    //% "Please wait"
    property string dialogHomingTitle: qsTrId("dialog-homing-title")

    //% "Performing homing sequence..."
    property string dialogHomingText: qsTrId("dialog-homing-text")

    //% "Please confirm action"
    property string dialogCloseIdleTitle: qsTrId("dialog-close-idle-title")

    //% "Do you really want to quit ?"
    property string dialogCloseIdleText: qsTrId("dialog-close-idle-text")

    //% "Action restricted"
    property string dialogCloseRunningTitle: qsTrId("dialog-close-running-title")

    //% "Can not quit while jobs are running"
    property string dialogCloseRunningText: qsTrId("dialog-close-running-text")

    //% "Use \"Reset\" button to abort job"
    property string dialogCloseRunningInfo: qsTrId("dialog-close-running-info")

    //% "Unsupported GRBL version"
    property string dialogUnsupportedGrblTitle: qsTrId("dialog-unsupported-grbl-title")

    //% "This GRBL version is not supported, only auxiliary features will be available.\n Please upgrade to Grbl 1.1 or later"
    property string dialogUnsupportedGrblText: qsTrId("dialog-unsupported-grbl-text")



    //: Label of the milimeter unit
    //% "(mm)"
    property string labelUnitMm: qsTrId("label-unit-mm")

    //: Label of the inch unit
    //% "(in)"
    property string labelUnitMm: qsTrId("label-unit-in")


    //: Title of the job file dialog
    //% "Please choose a file"
    property string labelUnitMm: qsTrId("dialog-select-job-file-title")
}
