import QtQuick 2.0

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

import StyleFile 1.0

Rectangle {
    property string imageSource ;
    property color overlayColorIdle: Style.customButton.defaultColor;
    property color overlayColorDisabled: Qt.darker(overlayColorIdle);
    property color overlayColorPressed : Qt.darker(overlayColorIdle);
    property color overlayColorTabActive : overlayColorPressed;
    property color backgroundColorIdle: "transparent"
    property color backgroundColorClicked: "transparent";
    property color backgroundColorEngaged: backgroundColorClicked;
    property color textColorIdle: Style.customButton.defaultColor;
    property color textColorDisabled: Qt.darker(textColorIdle);
    property bool engaged: false
    property alias badge : badgeContainer.data
    property alias caption : captionText.text
    signal clicked()

    id:customButton

    height: width

    color: engaged ? (backgroundColorEngaged) :
                        ( mouseArea.pressed ? backgroundColorClicked : backgroundColorIdle );

    MouseArea {
        id:mouseArea
        anchors.fill: parent
        onClicked: {
            if(customButton.enabled){
                parent.clicked()
            }
        }
    }

    Item {
        id: icon
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: caption === "" ? 0 : -parent.height * 0.1
        height: caption === "" ? parent.height : parent.height * 0.7
        width: height


        Image {
            id: buttonIcon
            source: imageSource
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * 0.8
            width: parent.width * 0.8
            sourceSize.height: height   //Appears to make image look better
            sourceSize.width: width     //Appears to make image look better
            fillMode: Image.PreserveAspectFit
            smooth: true
            visible: false
        }

        ColorOverlay {
            id:buttonIconOverlaid
            anchors.fill: buttonIcon
            source: buttonIcon
            color: getOverlayColor()
            visible: false

            function getOverlayColor() {
                if(!customButton.enabled)
                    return overlayColorDisabled
                else if (customButton.engaged)
                    return overlayColorTabActive
                else if (mouseArea.pressed)
                    return overlayColorPressed
                else
                    return overlayColorIdle
            }
        }

        DropShadow {
            source: buttonIconOverlaid
            anchors.fill: buttonIcon
            horizontalOffset: getOffset()
            verticalOffset: horizontalOffset
            radius: getRadius()
            samples: 21 //< Do no edit this value, other values can cause crash on windows
            scale: getScale()

            function getOffset() {
                var minDimension = Math.min(width,height)
                if(customButton.enabled != true)
                    return 0
                else if (mouseArea.pressed)
                    return minDimension / 40
                else
                    return minDimension/ 20
            }

            function getScale() {
                if(mouseArea.pressed && (customButton.enabled === true))
                    return 0.95
                else
                    return 1.0
            }

            function getRadius() {
                if(mouseArea.pressed && (customButton.enabled === true))
                    return 4.0
                else
                    return 8.0
            }
        }

        Item {
            id:badgeContainer
            width:parent.width * 0.45
            height: width
            anchors.left: parent.horizontalCenter
            anchors.top:parent.verticalCenter
            opacity: 0.8
        }
    }



    Text {
        id:captionText
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width * 0.9
        y: parent.height * 0.7
        height: parent.height * 0.2
        color: getTextColor()
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height * Style.customButton.textPixelSizeFactor
        font.bold: true

        function getTextColor() {
            if(customButton.enabled === true)
                return textColorIdle
            else
                return textColorDisabled
        }
    }
}
