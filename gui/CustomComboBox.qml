import QtQuick 2.0

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

import StyleFile 1.0

ComboBox{
    style: ComboBoxStyle {

        label: Item {
            Text {
                color: control.enabled ? Style.input.enabledColor : Style.input.disabledColor
                text: control.currentText
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                font.bold: true
                font.pixelSize: control.height*0.5
                anchors.fill:parent
                anchors.leftMargin: parent.width * 0.05
            }
        }
        background: Rectangle {
            id:backgroundRect
            color: Style.input.bgColor

            Image {
                id: dropDownIcon
                source: "qrc:/icons/combobox/Expand"

                anchors.top:parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                width: height
                visible: false
            }

            ColorOverlay {
                id:iconOverlay
                anchors.fill: dropDownIcon
                source: dropDownIcon
                color: control.enabled ? Style.input.enabledColor : Style.input.disabledColor
                visible: true
            }
        }
    }
}
