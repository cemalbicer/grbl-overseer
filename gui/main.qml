import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import OpenGLVisualizerItem 1.0
import QtQuick.Window 2.2
import GrblStatus 1.0
import GrblBoard 1.0
import AppLogic 1.0

import StyleFile 1.0

Window {
    id: mainWindow
    objectName: "mainWindow"
    minimumWidth: 640
    minimumHeight: 480
    width: 640
    height: 480
    visible: true

    function limitPhysicalSize(wantedSize, maxPhysicalSize)
    {
        return Math.min(wantedSize,
                        Screen.pixelDensity * maxPhysicalSize)
    }

    TopBar {
        id: topBar
        anchors.top:parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height / 16.0
    }

    Item {
        id: mainPanel
        anchors.top: topBar.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        VisualizerItem {
            id:visualizer
            objectName: "visualizer"
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.left: sideBar.right

            Loader {
                active: (typeof(messageHandler)=="undefined") ? false : true //Only load if message handler exists
                anchors.fill: parent
                sourceComponent: debugTextAreaComponent

                Component {
                    id: debugTextAreaComponent
                    TextEdit {
                        id: debugTextArea
                        anchors.fill: parent
                        color: Style.text.defaultColor
                        text: "debug"
                        enabled: false // to disable mouse / kb / focus

                        Connections {
                            target: messageHandler
                            onNewMessage:
                            {
                                debugTextArea.insert(0,message)
                            }
                        }
                    }
                }
            }
        }


        SidePanel {
            id: sidePanel
            color: Style.sidePanel.bgColor
            anchors.top: sideBar.top
            anchors.bottom: sideBar.bottom
            anchors.left: sideBar.right
            clip: true

            //On vertical screen orientatio, occupy the whole screen
            deployedWidth: (Screen.primaryOrientation === Qt.PortraitOrientation)
                           ? visualizer.width
                           : parent.height * 0.45

            retractedWidth: 0
            state: tabs.currentIndex < 0 ? "retracted" : "deployed"

            MouseArea {
                anchors.fill: parent    //To capture events
                acceptedButtons: Qt.AllButtons
                preventStealing: true
                propagateComposedEvents: false

                onWheel: {
                    wheel.accepted = true
                }
            }

            Rectangle {
                id: titleRectangle
                anchors.top : parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: sidePanel.deployedWidth * 0.15
                color: Style.sidePanel.title.bgColor

                Text {
                    anchors.margins: parent.height / 20
                    anchors.fill: parent
                    text : titleOfCurrentTab()
                    color: Style.sidePanel.title.textColor
                    //fontSizeMode: Text.Fit
                    minimumPointSize: 2
                    font.pixelSize: parent.height * 0.6
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    function titleOfCurrentTab(){
                        var currentTab = tabs.getTab(tabs.currentIndex);
                        return currentTab ? currentTab.title : ""
                    }
                }
            }

            TabView {
                id: tabs
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top:titleRectangle.bottom
                anchors.bottom: parent.bottom
                tabsVisible: false
                currentIndex: -1

                style: TabViewStyle {
                    frameOverlap: 1
                    tabsAlignment: Qt.AlignHCenter
                    frame: Rectangle { color: sidePanel.color }
                }

                Tab {
                    id: tabJobs
                    //: The label of the jobs tab
                    //% "Jobs"
                    title: qsTrId("tab-jobs")
                    active:true //Force loading tab on startup
                    PanelJobs {
                        id: panelJobs
                    }
                }

                Tab {
                    id: tabControls
                    //: The label of the controls tab
                    //% "Controls"
                    title: qsTrId("tab-controls")
                    active:true //Force loading tab on startup
                    PanelControls{
                        id: panelControls
                    }
                }

                Tab {
                    id: tabMonitor
                    //: The label of the monitor tab
                    //% "Monitor"
                    title: qsTrId("tab-monitor")
                    active:true //Force loading tab on startup
                    PanelMonitor{
                        id: panelMonitor
                    }
                }

                Tab {
                    id: tabSettings
                    //: The label of the settings tab
                    //% "Settings"
                    title: qsTrId("tab-settings")
                    active:true //Force loading tab on startup
                    PanelSettings{
                        id: panelSettings
                    }
                }
            }
        }

        Rectangle {
            id: sideBar
            color: Style.sideBar.bgColor
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: limitPhysicalSize(parent.height / 8.0, 30.0)

            function isTabOpen(tabNum) {
                return tabs.currentIndex == tabNum
            }

            function toggleTab(tabNum)
            {
                tabs.currentIndex = (tabs.currentIndex == tabNum) ? -1 : tabNum
                if(tabs.currentIndex >= 0) {
                    tabs.getTab(tabs.currentIndex).item.forceActiveFocus();
                }
                else {
                    forceActiveFocus();
                }
            }

            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource: "qrc:/icons/tab/Jobs"
                    engaged: sideBar.isTabOpen(0)
                    onClicked: sideBar.toggleTab(0)
                    overlayColorPressed: Style.sideBar.buttonColor.jobs
                    backgroundColorEngaged: Style.sideBar.buttonColor.engaged
                    caption: qsTrId("tab-jobs")
                }

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource:  "qrc:/icons/tab/Controls"
                    engaged: sideBar.isTabOpen(1)
                    onClicked: sideBar.toggleTab(1)
                    overlayColorPressed: Style.sideBar.buttonColor.controls
                    backgroundColorEngaged: Style.sideBar.buttonColor.engaged
                    caption: qsTrId("tab-controls")
                }

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource:  "qrc:/icons/tab/Monitor"
                    engaged: sideBar.isTabOpen(2)
                    onClicked: sideBar.toggleTab(2)
                    overlayColorPressed: Style.sideBar.buttonColor.monitor
                    backgroundColorEngaged: Style.sideBar.buttonColor.engaged
                    caption: qsTrId("tab-monitor")
                }

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource: "qrc:/icons/tab/Settings"
                    engaged: sideBar.isTabOpen(3)
                    onClicked: sideBar.toggleTab(3)
                    overlayColorPressed: Style.sideBar.buttonColor.settings;
                    backgroundColorEngaged: Style.sideBar.buttonColor.engaged
                    caption: qsTrId("tab-settings")
                }

                Action {
                    shortcut: "F1"
                    onTriggered: sideBar.toggleTab(0)
                }
                Action {
                    shortcut: "F2"
                    onTriggered: sideBar.toggleTab(1)
                }
                Action {
                    shortcut: "F3"
                    onTriggered: sideBar.toggleTab(2)
                }
                Action {
                    shortcut: "F4"
                    onTriggered: sideBar.toggleTab(3)
                }
            }

            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom

                TabView {
                    id:playPauseButton
                    width: parent.width
                    height:width
                    tabsVisible: false
                    currentIndex: getProperButtonIndex();

                    style: TabViewStyle {
                        frameOverlap: 1
                        tabsAlignment: Qt.AlignHCenter
                        frame: Rectangle { color: "transparent" }
                    }


                    Tab{
                        CustomButton {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            imageSource:  "qrc:/icons/cmdPlay"
                            overlayColorIdle: (skipSimulationEnabled === true)
                                              ? Style.sideBar.buttonColor.startSkipSim
                                              : Style.sideBar.buttonColor.start
                            overlayColorDisabled: Style.sideBar.buttonColor.disabled
                            caption: (skipSimulationEnabled === true) ? qsTrId("button-start-no-sim") : qsTrId("button-start")
                            enabled: appLogic.readyToGo
                            onClicked: {
                                if(skipSimulationEnabled === true) appLogic.startProduction()
                                else appLogic.startSimulation();
                            }
                        }
                    }

                    Tab{
                        CustomButton {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            imageSource:  "qrc:/icons/cmdPlay"
                            overlayColorIdle: Style.sideBar.buttonColor.resume
                            caption: qsTrId("button-resume")
                            onClicked: {
                                grblBoard.resume();
                            }
                        }
                    }

                    Tab {
                        CustomButton {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            imageSource:  "qrc:/icons/cmdPause"
                            overlayColorIdle: Style.sideBar.buttonColor.idle
                            caption: qsTrId("button-pause")
                            onClicked: {
                                grblBoard.hold();
                            }
                        }
                    }

                    function getProperButtonIndex(){
                        if(grblStatus.currentState === GrblStatus.State_Hold || grblStatus.currentState === GrblStatus.State_Door)
                            return 1
                        //We also need to test if running, because if grbl buffer is starved, it will show in IDLE state
                        else if(grblStatus.currentState === GrblStatus.State_Run || appLogic.currentState === AppLogic.State_Production_Running)
                            return 2
                        else
                            return 0
                    }
                }

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource: "qrc:/icons/cmdReset"
                    overlayColorIdle: Style.sideBar.buttonColor.reset
                    overlayColorDisabled: Style.sideBar.buttonColor.disabled

                    enabled: (grblStatus.currentState !== GrblStatus.State_Offline)
                    caption: qsTrId("button-reset")
                    onClicked: {
                        appLogic.abort();
                    }
                }
            }
        }
    }


    CustomDialog {
        id: errorOpeningJobDialog
        type:typeCritical
        title: qsTrId("dialog-open-job-error-title")
        text: qsTrId("dialog-open-job-error-text")
        informativeText: qsTrId("dialog-open-job-error-info")
        buttons: buttonOk
    }

    Connections{
        target: jobManager
        onJobLoadFailed:
        {
            errorOpeningJobDialog.detailedText = error
            errorOpeningJobDialog.open()
        }
    }

    CustomDialog {
        id: productionCompletedDialog
        type: typeInfo
        title: qsTrId("dialog-all-jobs-complete-title")
        onAccepted: appLogic.abort()
    }

    CustomDialog {
        id: simulationCompletedWithErrorsDialog
        type:typeWarning
        buttons: buttonYes | buttonNo
        title: qsTrId("dialog-job-sim-error-title")
        text: qsTrId("dialog-job-sim-error-text")
        informativeText: qsTrId("dialog-job-sim-error-info")
        onYes: appLogic.startProduction()
        onAccepted: appLogic.startProduction()

        onNo: appLogic.abort()
    }

    CustomDialog {
        id: simulationCompletedWithoutErrorsDialog
        type: typeInfo
        buttons: buttonYes | buttonNo
        title: qsTrId("dialog-job-sim-complete-title")
        text: qsTrId("dialog-job-sim-complete-text")

        onYes: appLogic.startProduction()
        onAccepted: appLogic.startProduction()

        onNo: appLogic.abort()
    }

    CustomDialog {
        id: errorDuringProductionDialog
        type:typeCritical
        title: qsTrId("dialog-error-during-production-title")
        text: qsTrId("dialog-error-during-production-text")
        informativeText: qsTrId("dialog-error-during-production-info")
        buttons: buttonYes | buttonNo

        onYes: grblBoard.resume()
        onAccepted: grblBoard.resume()

        onNo: appLogic.abort()
    }

    CustomDialog {
        id: alarmDialog
        type:typeCritical
        title: qsTrId("dialog-alarm-title")
        text: qsTrId("dialog-alarm-text")
        informativeText: qsTrId("dialog-alarm-info")
        onAccepted: appLogic.abort()
    }

    Connections{
        target: appLogic

        onProductionCompleted: {
            productionCompletedDialog.open()
        }

        onProductionError: {
            errorDuringProductionDialog.detailedText = errorMsg
            errorDuringProductionDialog.open()
        }

        onSimulationCompleted: {
            if(appLogic.getErrorCount() === 0)
            {
                simulationCompletedWithoutErrorsDialog.open()
            }
            else
            {
                simulationCompletedWithErrorsDialog.detailedText = appLogic.getRegisteredErrors()
                simulationCompletedWithErrorsDialog.open()
            }
        }
    }

    Connections{
        target: grblBoard
        onAlarm: {
            alarmDialog.detailedText = alarmMsg
            alarmDialog.open()
        }
        onBoardVersionNotSupported:{
            boardVersionNotSupportedDialog.open()
        }
    }

    CustomDialog {
        id:homingDialog
        type: typeWarning
        title: qsTrId("dialog-homing-title")
        text: qsTrId("dialog-homing-text")
        buttons: buttonAbort

        visible: grblBoard.homingState === GrblBoard.Homing_Running

        onRejected: appLogic.abort()
    }

    CustomDialog {
        id: closeWhenIdleDialog
        type: typeWarning
        title: qsTrId("dialog-close-idle-title")
        text: qsTrId("dialog-close-idle-text")
        buttons: buttonYes | buttonNo
        onYes: Qt.quit()
        onAccepted: Qt.quit()
    }

    CustomDialog {
        id: closeWhenRunningDialog
        type: typeCritical
        title: qsTrId("dialog-close-running-title")
        text: qsTrId("dialog-close-running-text")
        informativeText: qsTrId("dialog-close-running-info")
    }

    CustomDialog {
        id: boardVersionNotSupportedDialog
        type: typeWarning
        title: qsTrId("dialog-unsupported-grbl-title")
        text: qsTrId("dialog-unsupported-grbl-text")
    }

    onClosing: {
        close.accepted = false
        appLogic.isIdle ? closeWhenIdleDialog.open() : closeWhenRunningDialog.open()
    }
    Action {
        shortcut: "F11"
        onTriggered:
            mainWindow.visibility === Window.FullScreen
                     ? mainWindow.visibility = Window.Windowed
                     : mainWindow.visibility = Window.FullScreen
    }
    Action {
        shortcut: "F12"
        onTriggered: mainWindow.close()
    }
}

