<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name></name>
    <message id="dialog-button-ok">
        <location filename="../CustomDialog.qml" line="239"/>
        <location filename="../LocalizedStrings.qml" line="6"/>
        <source>Ok</source>
        <extracomment>The Ok button in generic dialogs</extracomment>
        <translation>Ok</translation>
    </message>
    <message id="dialog-button-yes">
        <location filename="../CustomDialog.qml" line="316"/>
        <location filename="../LocalizedStrings.qml" line="10"/>
        <source>Yes</source>
        <extracomment>The Yes button in generic dialogs</extracomment>
        <translation>Yes</translation>
    </message>
    <message id="dialog-button-no">
        <location filename="../CustomDialog.qml" line="330"/>
        <location filename="../LocalizedStrings.qml" line="14"/>
        <source>No</source>
        <extracomment>The No button in generic dialogs</extracomment>
        <translation>No</translation>
    </message>
    <message id="dialog-button-abort">
        <location filename="../CustomDialog.qml" line="344"/>
        <location filename="../LocalizedStrings.qml" line="18"/>
        <source>Abort</source>
        <extracomment>The Abort button in generic dialogs</extracomment>
        <translation>Abort</translation>
    </message>
    <message id="grbl-settings-value-placeholder">
        <location filename="../GRBLSettingsDelegate.qml" line="124"/>
        <location filename="../GRBLSettingsDelegate.qml" line="154"/>
        <location filename="../LocalizedStrings.qml" line="22"/>
        <source>Value</source>
        <extracomment>The placeholder in GRBL config value field</extracomment>
        <translation>Value</translation>
    </message>
    <message id="grbl-settings-value-bool-no">
        <location filename="../GRBLSettingsDelegate.qml" line="145"/>
        <location filename="../LocalizedStrings.qml" line="30"/>
        <source>No</source>
        <extracomment>The label for &quot;false&quot; in GRBL config bool field</extracomment>
        <translation>No</translation>
    </message>
    <message id="grbl-settings-value-bool-yes">
        <location filename="../GRBLSettingsDelegate.qml" line="145"/>
        <location filename="../LocalizedStrings.qml" line="26"/>
        <source>Yes</source>
        <extracomment>The label for &quot;true&quot; in GRBL config bool field</extracomment>
        <translation>Yes</translation>
    </message>
    <message id="label-lines-count">
        <location filename="../JobDelegate.qml" line="61"/>
        <location filename="../LocalizedStrings.qml" line="34"/>
        <source>%1 lines</source>
        <extracomment>The line count in a task</extracomment>
        <translation>%1 lines</translation>
    </message>
    <message id="job-button-set-origin-here">
        <location filename="../JobDelegate.qml" line="162"/>
        <location filename="../LocalizedStrings.qml" line="38"/>
        <source>Set origin here</source>
        <extracomment>The label of the &quot;Set origin here&quot; job button</extracomment>
        <translation>Set origin here</translation>
    </message>
    <message id="job-button-randomise-color">
        <location filename="../JobDelegate.qml" line="192"/>
        <location filename="../LocalizedStrings.qml" line="42"/>
        <source>Change color</source>
        <extracomment>The label of the &quot;Change color&quot; job button</extracomment>
        <translation>Change color</translation>
    </message>
    <message id="job-button-delete">
        <location filename="../JobDelegate.qml" line="220"/>
        <location filename="../LocalizedStrings.qml" line="46"/>
        <source>Delete job</source>
        <extracomment>The label of the &quot;Delete job&quot; job button</extracomment>
        <translation>Delete job</translation>
    </message>
    <message id="job-label-origin">
        <location filename="../JobDelegate.qml" line="248"/>
        <source>Job origin :</source>
        <extracomment>The label of the job origin field group</extracomment>
        <translation>Job origin :</translation>
    </message>
    <message id="label-unit-mm">
        <location filename="../JobDelegate.qml" line="250"/>
        <location filename="../LocalizedStrings.qml" line="297"/>
        <location filename="../TopBar.qml" line="107"/>
        <source>(mm)</source>
        <extracomment>Label of the milimeter unit</extracomment>
        <translation>(mm)</translation>
    </message>
    <message id="axis-label-x">
        <location filename="../JobDelegate.qml" line="267"/>
        <location filename="../LocalizedStrings.qml" line="50"/>
        <source>X</source>
        <extracomment>The label of the X axis</extracomment>
        <translation>X</translation>
    </message>
    <message id="axis-label-y">
        <location filename="../JobDelegate.qml" line="275"/>
        <location filename="../LocalizedStrings.qml" line="54"/>
        <source>Y</source>
        <extracomment>The label of the Y axis</extracomment>
        <translation>Y</translation>
    </message>
    <message id="axis-label-z">
        <location filename="../JobDelegate.qml" line="283"/>
        <location filename="../LocalizedStrings.qml" line="58"/>
        <source>Z</source>
        <extracomment>The label of the Z axis</extracomment>
        <translation>Z</translation>
    </message>
    <message id="axis-label-y-positive">
        <location filename="../JogControls.qml" line="132"/>
        <location filename="../LocalizedStrings.qml" line="70"/>
        <source>Y+</source>
        <extracomment>The label of the Y+ axis</extracomment>
        <translation>Y+</translation>
    </message>
    <message id="axis-label-y-negative">
        <location filename="../JogControls.qml" line="147"/>
        <location filename="../LocalizedStrings.qml" line="74"/>
        <source>Y-</source>
        <extracomment>The label of the Y- axis</extracomment>
        <translation>Y-</translation>
    </message>
    <message id="axis-label-x-negative">
        <location filename="../JogControls.qml" line="162"/>
        <location filename="../LocalizedStrings.qml" line="66"/>
        <source>X-</source>
        <extracomment>The label of the X- axis</extracomment>
        <translation>X-</translation>
    </message>
    <message id="axis-label-x-positive">
        <location filename="../JogControls.qml" line="177"/>
        <location filename="../LocalizedStrings.qml" line="62"/>
        <source>X+</source>
        <extracomment>The label of the X+ axis</extracomment>
        <translation>X+</translation>
    </message>
    <message id="axis-label-z-positive">
        <location filename="../JogControls.qml" line="275"/>
        <location filename="../LocalizedStrings.qml" line="78"/>
        <source>Z+</source>
        <extracomment>The label of the Z+ axis</extracomment>
        <translation>Z+</translation>
    </message>
    <message id="axis-label-z-negative">
        <location filename="../JogControls.qml" line="288"/>
        <location filename="../LocalizedStrings.qml" line="82"/>
        <source>Z-</source>
        <extracomment>The label of the Z- axis</extracomment>
        <translation>Z-</translation>
    </message>
    <message id="label-jog-group">
        <location filename="../LocalizedStrings.qml" line="86"/>
        <location filename="../PanelControls.qml" line="32"/>
        <source>Jog</source>
        <extracomment>The label of the jog control group title</extracomment>
        <translation>Jog</translation>
    </message>
    <message id="label-manual-control-group">
        <location filename="../LocalizedStrings.qml" line="90"/>
        <location filename="../PanelControls.qml" line="55"/>
        <source>Manual Commands</source>
        <extracomment>The manual control group title</extracomment>
        <translation>Manual Commands</translation>
    </message>
    <message id="button-homing">
        <location filename="../LocalizedStrings.qml" line="93"/>
        <location filename="../PanelControls.qml" line="68"/>
        <source>Run homing</source>
        <translation>Run homing</translation>
    </message>
    <message id="button-alarm-reset">
        <location filename="../LocalizedStrings.qml" line="96"/>
        <location filename="../PanelControls.qml" line="92"/>
        <source>Kill alarm</source>
        <translation>Kill alarm</translation>
    </message>
    <message id="label-overrides-group">
        <location filename="../LocalizedStrings.qml" line="99"/>
        <location filename="../PanelControls.qml" line="110"/>
        <source>Overrides</source>
        <translation>Overrides</translation>
    </message>
    <message id="label-feed-rate">
        <location filename="../LocalizedStrings.qml" line="102"/>
        <location filename="../PanelControls.qml" line="129"/>
        <source>Feed motions</source>
        <translation>Feed motions</translation>
    </message>
    <message id="label-move-rate">
        <location filename="../LocalizedStrings.qml" line="105"/>
        <location filename="../PanelControls.qml" line="170"/>
        <source>Rapid motions</source>
        <translation>Rapid motions</translation>
    </message>
    <message id="label-spin-rate">
        <location filename="../LocalizedStrings.qml" line="108"/>
        <location filename="../PanelControls.qml" line="199"/>
        <source>Spindle RPM</source>
        <translation>Spindle RPM</translation>
    </message>
    <message id="button-load-more-jobs">
        <location filename="../LocalizedStrings.qml" line="111"/>
        <location filename="../PanelJobs.qml" line="36"/>
        <source>Load more G-Code jobs</source>
        <translation>Load more G-Code jobs</translation>
    </message>
    <message id="button-load-job">
        <location filename="../LocalizedStrings.qml" line="114"/>
        <location filename="../PanelJobs.qml" line="36"/>
        <source>Load G-Code jobs</source>
        <translation>Load G-Code jobs</translation>
    </message>
    <message id="label-monitor-plan-buffer-error">
        <location filename="../LocalizedStrings.qml" line="119"/>
        <location filename="../PanelMonitor.qml" line="63"/>
        <source>Plan Buf : Err</source>
        <translation>Plan Buf : Err</translation>
    </message>
    <message id="label-monitor-plan-buffer">
        <location filename="../LocalizedStrings.qml" line="122"/>
        <location filename="../PanelMonitor.qml" line="65"/>
        <source>Plan Buf : </source>
        <translation>Plan Buf : </translation>
    </message>
    <message id="label-monitor-char-buffer-error">
        <location filename="../LocalizedStrings.qml" line="125"/>
        <location filename="../PanelMonitor.qml" line="100"/>
        <source>Char Buf : Err</source>
        <translation>Char Buf : Err</translation>
    </message>
    <message id="label-monitor-char-buffer">
        <location filename="../LocalizedStrings.qml" line="128"/>
        <location filename="../PanelMonitor.qml" line="102"/>
        <source>Char Buf : </source>
        <translation>Char Buf : </translation>
    </message>
    <message id="input-field-manual-command">
        <location filename="../LocalizedStrings.qml" line="131"/>
        <location filename="../PanelMonitor.qml" line="154"/>
        <source>Type manual commands here</source>
        <translation>Type manual commands here</translation>
    </message>
    <message id="label-serial-link-group">
        <location filename="../LocalizedStrings.qml" line="134"/>
        <location filename="../PanelSettings.qml" line="13"/>
        <source>Serial Link</source>
        <translation>Serial Link</translation>
    </message>
    <message id="button-serial-link-disconnect">
        <location filename="../LocalizedStrings.qml" line="137"/>
        <location filename="../PanelSettings.qml" line="66"/>
        <source>Disconnect</source>
        <translation>Disconnect</translation>
    </message>
    <message id="button-serial-link-connect">
        <location filename="../LocalizedStrings.qml" line="140"/>
        <location filename="../PanelSettings.qml" line="66"/>
        <source>Connect</source>
        <translation>Connect</translation>
    </message>
    <message id="label-grbl-conf-group">
        <location filename="../LocalizedStrings.qml" line="143"/>
        <location filename="../PanelSettings.qml" line="89"/>
        <source>GRBL Configuration</source>
        <translation>GRBL Configuration</translation>
    </message>
    <message id="label-settings-machine-config-not-available">
        <location filename="../LocalizedStrings.qml" line="146"/>
        <location filename="../PanelSettings.qml" line="121"/>
        <source>Machine
configuration
not available</source>
        <translation>Machine
configuration
not available</translation>
    </message>
    <message id="label-coordinates-work">
        <location filename="../LocalizedStrings.qml" line="149"/>
        <location filename="../TopBar.qml" line="108"/>
        <source>Work.</source>
        <translation>Work.</translation>
    </message>
    <message id="label-coordinates-machine">
        <location filename="../LocalizedStrings.qml" line="152"/>
        <location filename="../TopBar.qml" line="108"/>
        <source>Mach.</source>
        <translation>Mach.</translation>
    </message>
    <message id="label-status-title">
        <location filename="../LocalizedStrings.qml" line="157"/>
        <location filename="../TopBar.qml" line="123"/>
        <source>Grbl State</source>
        <translation>Grbl State</translation>
    </message>
    <message id="label-grbl-status-idle">
        <location filename="../LocalizedStrings.qml" line="160"/>
        <location filename="../TopBar.qml" line="130"/>
        <source>Idle</source>
        <translation>Idle</translation>
    </message>
    <message id="label-grbl-status-running">
        <location filename="../LocalizedStrings.qml" line="163"/>
        <location filename="../TopBar.qml" line="132"/>
        <source>Running</source>
        <translation>Running</translation>
    </message>
    <message id="label-grbl-status-paused ">
        <location filename="../LocalizedStrings.qml" line="166"/>
        <location filename="../TopBar.qml" line="134"/>
        <source>Paused</source>
        <translation>Paused</translation>
    </message>
    <message id="label-grbl-status-jogging">
        <location filename="../LocalizedStrings.qml" line="169"/>
        <location filename="../TopBar.qml" line="136"/>
        <source>Jogging</source>
        <translation>Jogging</translation>
    </message>
    <message id="label-grbl-status-door">
        <location filename="../LocalizedStrings.qml" line="172"/>
        <location filename="../TopBar.qml" line="138"/>
        <source>Door</source>
        <translation>Door</translation>
    </message>
    <message id="label-grbl-status-homing">
        <location filename="../LocalizedStrings.qml" line="175"/>
        <location filename="../TopBar.qml" line="140"/>
        <source>Homing</source>
        <translation>Homing</translation>
    </message>
    <message id="label-grbl-status-alarm">
        <location filename="../LocalizedStrings.qml" line="178"/>
        <location filename="../TopBar.qml" line="142"/>
        <source>Alarm</source>
        <translation>Alarm</translation>
    </message>
    <message id="label-grbl-status-simulation">
        <location filename="../LocalizedStrings.qml" line="181"/>
        <location filename="../TopBar.qml" line="144"/>
        <source>Simulation</source>
        <translation>Simulation</translation>
    </message>
    <message id="label-grbl-status-sleeping">
        <location filename="../LocalizedStrings.qml" line="184"/>
        <location filename="../TopBar.qml" line="146"/>
        <source>Sleeping</source>
        <translation>Sleeping</translation>
    </message>
    <message id="label-grbl-status-unknown">
        <location filename="../LocalizedStrings.qml" line="187"/>
        <location filename="../TopBar.qml" line="148"/>
        <source>Status unknown</source>
        <translation>Status unknown</translation>
    </message>
    <message id="label-grbl-status-disconnected">
        <location filename="../LocalizedStrings.qml" line="190"/>
        <location filename="../TopBar.qml" line="150"/>
        <source>Not connected</source>
        <translation>Not connected</translation>
    </message>
    <message id="tab-jobs">
        <location filename="../LocalizedStrings.qml" line="194"/>
        <location filename="../main.qml" line="150"/>
        <location filename="../main.qml" line="228"/>
        <source>Jobs</source>
        <extracomment>The label of the jobs tab</extracomment>
        <translation>Jobs</translation>
    </message>
    <message id="tab-controls">
        <location filename="../LocalizedStrings.qml" line="197"/>
        <location filename="../main.qml" line="161"/>
        <location filename="../main.qml" line="239"/>
        <source>Controls</source>
        <extracomment>The label of the controls tab</extracomment>
        <translation>Controls</translation>
    </message>
    <message id="tab-monitor">
        <location filename="../LocalizedStrings.qml" line="200"/>
        <location filename="../main.qml" line="172"/>
        <location filename="../main.qml" line="250"/>
        <source>Monitor</source>
        <extracomment>The label of the monitor tab</extracomment>
        <translation>Monitor</translation>
    </message>
    <message id="tab-settings">
        <location filename="../LocalizedStrings.qml" line="203"/>
        <location filename="../main.qml" line="183"/>
        <location filename="../main.qml" line="261"/>
        <source>Settings</source>
        <extracomment>The label of the settings tab</extracomment>
        <translation>Settings</translation>
    </message>
    <message id="button-start-no-sim">
        <location filename="../LocalizedStrings.qml" line="207"/>
        <location filename="../main.qml" line="310"/>
        <source>Go
(No Sim.)</source>
        <translation>Go
(No Sim.)</translation>
    </message>
    <message id="button-start">
        <location filename="../LocalizedStrings.qml" line="210"/>
        <location filename="../main.qml" line="310"/>
        <source>Go</source>
        <translation>Go</translation>
    </message>
    <message id="button-resume">
        <location filename="../LocalizedStrings.qml" line="213"/>
        <location filename="../main.qml" line="325"/>
        <source>Resume</source>
        <translation>Resume</translation>
    </message>
    <message id="button-pause">
        <location filename="../LocalizedStrings.qml" line="216"/>
        <location filename="../main.qml" line="338"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message id="button-reset">
        <location filename="../LocalizedStrings.qml" line="219"/>
        <location filename="../main.qml" line="364"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message id="dialog-open-job-error-title">
        <location filename="../LocalizedStrings.qml" line="222"/>
        <location filename="../main.qml" line="377"/>
        <source>Error opening job</source>
        <translation>Error opening job</translation>
    </message>
    <message id="dialog-open-job-error-text">
        <location filename="../LocalizedStrings.qml" line="225"/>
        <location filename="../main.qml" line="378"/>
        <source>An error was encountered while trying to open a job</source>
        <translation>An error was encountered while trying to open a job</translation>
    </message>
    <message id="dialog-open-job-error-info">
        <location filename="../LocalizedStrings.qml" line="228"/>
        <location filename="../main.qml" line="379"/>
        <source>Error message :</source>
        <translation>Error message :</translation>
    </message>
    <message id="dialog-all-jobs-complete-title">
        <location filename="../LocalizedStrings.qml" line="231"/>
        <location filename="../main.qml" line="395"/>
        <source>All jobs completed !</source>
        <translation>All jobs completed !</translation>
    </message>
    <message id="dialog-job-sim-error-title">
        <location filename="../LocalizedStrings.qml" line="234"/>
        <location filename="../main.qml" line="403"/>
        <source>Simulation completed with errors</source>
        <translation>Simulation completed with errors</translation>
    </message>
    <message id="dialog-job-sim-error-text">
        <location filename="../LocalizedStrings.qml" line="237"/>
        <location filename="../main.qml" line="404"/>
        <source>Start production anyway ?</source>
        <translation>Start production anyway ?</translation>
    </message>
    <message id="dialog-job-sim-error-info">
        <location filename="../LocalizedStrings.qml" line="240"/>
        <location filename="../main.qml" line="405"/>
        <source>Errors :</source>
        <translation>Errors :</translation>
    </message>
    <message id="dialog-job-sim-complete-title">
        <location filename="../LocalizedStrings.qml" line="243"/>
        <location filename="../main.qml" line="416"/>
        <source>Simulation completed without errors</source>
        <translation>Simulation completed without errors</translation>
    </message>
    <message id="dialog-job-sim-complete-text">
        <location filename="../LocalizedStrings.qml" line="246"/>
        <location filename="../main.qml" line="417"/>
        <source>Start production ?</source>
        <translation>Start production ?</translation>
    </message>
    <message id="dialog-error-during-production-title">
        <location filename="../LocalizedStrings.qml" line="249"/>
        <location filename="../main.qml" line="428"/>
        <source>Command Error</source>
        <translation>Command Error</translation>
    </message>
    <message id="dialog-error-during-production-text">
        <location filename="../LocalizedStrings.qml" line="252"/>
        <location filename="../main.qml" line="429"/>
        <source>Grbl just returned an error. Continue despite error ?</source>
        <translation>Grbl just returned an error. Continue despite error ?</translation>
    </message>
    <message id="dialog-error-during-production-info">
        <location filename="../LocalizedStrings.qml" line="255"/>
        <location filename="../main.qml" line="430"/>
        <source>Error message :</source>
        <translation>Error message :</translation>
    </message>
    <message id="dialog-alarm-title">
        <location filename="../LocalizedStrings.qml" line="258"/>
        <location filename="../main.qml" line="442"/>
        <source>Alarm</source>
        <translation>Alarm</translation>
    </message>
    <message id="dialog-alarm-text">
        <location filename="../LocalizedStrings.qml" line="261"/>
        <location filename="../main.qml" line="443"/>
        <source>Grbl is in Alarm state. Use Kill Alarm or Run homing commands in Controls tab to return to idle mode</source>
        <translation>Grbl is in Alarm state. Use Kill Alarm or Run homing commands in Controls tab to return to idle mode</translation>
    </message>
    <message id="dialog-alarm-info">
        <location filename="../LocalizedStrings.qml" line="264"/>
        <location filename="../main.qml" line="444"/>
        <source>Alarm message : </source>
        <translation>Alarm message : </translation>
    </message>
    <message id="dialog-homing-title">
        <location filename="../LocalizedStrings.qml" line="267"/>
        <location filename="../main.qml" line="487"/>
        <source>Please wait</source>
        <translation>Please wait</translation>
    </message>
    <message id="dialog-homing-text">
        <location filename="../LocalizedStrings.qml" line="270"/>
        <location filename="../main.qml" line="488"/>
        <source>Performing homing sequence...</source>
        <translation>Performing homing sequence...</translation>
    </message>
    <message id="dialog-close-idle-title">
        <location filename="../LocalizedStrings.qml" line="273"/>
        <location filename="../main.qml" line="499"/>
        <source>Please confirm action</source>
        <translation>Please confirm action</translation>
    </message>
    <message id="dialog-close-idle-text">
        <location filename="../LocalizedStrings.qml" line="276"/>
        <location filename="../main.qml" line="500"/>
        <source>Do you really want to quit ?</source>
        <translation>Do you really want to quit ?</translation>
    </message>
    <message id="dialog-close-running-title">
        <location filename="../LocalizedStrings.qml" line="279"/>
        <location filename="../main.qml" line="509"/>
        <source>Action restricted</source>
        <translation>Action restricted</translation>
    </message>
    <message id="dialog-close-running-text">
        <location filename="../LocalizedStrings.qml" line="282"/>
        <location filename="../main.qml" line="510"/>
        <source>Can not quit while jobs are running</source>
        <translation>Can not quit while jobs are running</translation>
    </message>
    <message id="dialog-close-running-info">
        <location filename="../LocalizedStrings.qml" line="285"/>
        <location filename="../main.qml" line="511"/>
        <source>Use &quot;Reset&quot; button to abort job</source>
        <translation>Use &quot;Reset&quot; button to abort job</translation>
    </message>
    <message id="dialog-unsupported-grbl-title">
        <location filename="../LocalizedStrings.qml" line="288"/>
        <location filename="../main.qml" line="517"/>
        <source>Unsupported GRBL version</source>
        <translation>Unsupported GRBL version</translation>
    </message>
    <message id="dialog-unsupported-grbl-text">
        <location filename="../LocalizedStrings.qml" line="291"/>
        <location filename="../main.qml" line="518"/>
        <source>This GRBL version is not supported, only auxiliary features will be available.
 Please upgrade to Grbl 1.1 or later</source>
        <translation>This GRBL version is not supported, only auxiliary features will be available.
 Please upgrade to Grbl 1.1 or later</translation>
    </message>
    <message id="label-unit-in">
        <location filename="../LocalizedStrings.qml" line="301"/>
        <location filename="../TopBar.qml" line="107"/>
        <source>(in)</source>
        <extracomment>Label of the inch unit</extracomment>
        <translation>(in)</translation>
    </message>
    <message id="dialog-select-job-file-title">
        <location filename="../LocalizedStrings.qml" line="306"/>
        <location filename="../PanelJobs.qml" line="73"/>
        <source>Please choose a file</source>
        <extracomment>Title of the job file dialog</extracomment>
        <translation>Please choose a file</translation>
    </message>
</context>
</TS>
