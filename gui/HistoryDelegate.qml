import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.4

import GrblHistory 1.0

import StyleFile 1.0

DeployableItem {
    id:historyItem

    property var subLinesModel : model.subLines

    mainHeight: mainLine.height
    extendedHeight: messagesListView.contentHeight


    mainItem: Item {
        id: mainLine;
        height: title.height * Style.history.mainLineHeightFactor
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

        Text {
            id: title
            text: model.mainLine
            color: Style.text.defaultColor
            font.family: "Helvetica"
            font.pixelSize: parent.width * Style.history.textPixelSizeFactor

            anchors.verticalCenter: parent.verticalCenter
            anchors.left:   statusArea.right
            anchors.right:  parent.right

            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
        }

        Item {
            id:statusArea
            anchors.left:   parent.left
            anchors.top:    parent.top
            anchors.bottom: parent.bottom

            width:  parent.width * Style.history.statusAreaWidthFactor

            Image {
                id:statusIcon
                width:  parent.width   * Style.history.statusIconSizeFactor
                height: width
                anchors.verticalCenter:     parent.verticalCenter
                anchors.horizontalCenter:   parent.horizontalCenter
                source:  getIconStatus(model.status)

                function getIconStatus (status)
                {
                    switch(status) {
                    case GrblHistory.Status_Sent:
                        return "qrc:/icons/monitor/Running";
                    case GrblHistory.Status_Ok:
                        return "qrc:/icons/monitor/Ok";
                    case GrblHistory.Status_Error:
                        return "qrc:/icons/monitor/Error";
                    case GrblHistory.Status_Aborted:
                        return "qrc:/icons/monitor/Cancelled";
                    case GrblHistory.Status_Alarm:
                        return "qrc:/icons/monitor/Alarm";
                    default:
                        return "";
                    }
                }
            }

            Rectangle {
                id: verticalLineTop
                width: mainLine.width * Style.history.lineWidthFactor
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top:statusIcon.bottom
                anchors.bottom: parent.bottom
                color: Style.separator.color
                visible: historyItem.deployed && messagesListView.contentHeight > 0
            }

        }
    }

    extendedItem: ListView {
        id: messagesListView

        anchors.fill: parent

        clip: false

        model:subLinesModel

        delegate: Item {

            height: detailsContent.height
            width: parent.width

            Text{
                id: detailsContent

                anchors.top:    parent.top
                anchors.left:   decorationItem.right
                anchors.right:  parent.right

                text: modelData

                font.pixelSize: parent.width * Style.history.subTextPixelSizeFactor

                color: Style.text.defaultColor
                wrapMode: Text.WordWrap
            }

            Item {
                id: decorationItem
                anchors.left:   parent.left
                anchors.top:    parent.top
                anchors.bottom: parent.bottom
                width: parent.width * Style.history.statusAreaWidthFactor

                Rectangle {
                    id: verticalLine

                    width: messagesListView.width * Style.history.lineWidthFactor
                    anchors.horizontalCenter: parent.horizontalCenter

                    anchors.top:    parent.top
                    anchors.bottom: isLastIndex() ? parent.verticalCenter : parent.bottom

                    color: Style.separator.color

                    function isLastIndex() {
                        return (model.index === (messagesListView.count -1) )
                    }
                }

                Rectangle {
                    id: horizontalLine

                    height: messagesListView.width * Style.history.lineWidthFactor
                    anchors.verticalCenter: parent.verticalCenter

                    width: parent.width / 4
                    anchors.left:   parent.horizontalCenter

                    color: Style.separator.color
                }
            }
        }

        MouseArea {
            onClicked: {
                historyItem.ListView.view.currentIndex = historyItem.deployed ? -1 : model.index;
            }

            anchors.fill: parent
        }
    }
}
