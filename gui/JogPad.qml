import QtQuick 2.0

import StyleFile 1.0

Rectangle {

    readonly property vector2d axesValues : enabled ? mouseArea.axesValues : Qt.vector2d(0,0)

    border.color: Style.controls.borderColor
    border.width: height * Style.controls.borderWidth
    color: Style.controls.bgColor

    MouseArea {
        id:mouseArea
        anchors.fill: parent
        hoverEnabled: true
        preventStealing: true

        readonly property vector2d axesValues: Qt.vector2d(computeValue(mouseX, width),
                                                           -computeValue(mouseY, height));


        function computeValue(mousePosition, maxPosition){
            var value = pressed ? ((2.0 * mousePosition / maxPosition) - 1.0) : 0.0
            if(value < -0.33) return -1
            else if(value > 0.33) return 1
            else return 0
        }
    }
}
