#ifndef GCODELINE_H
#define GCODELINE_H

#include <QString>
#include <QVector>
#include <QVector3D>

class GCodeParser;
class GCodeJob;

class GCodeJobLine
{
public:
    enum Status {
        Line_Ready,         //Can be edited
        Line_Queued,        //Planned to be sent
        Line_Sent,          //Sent to grbl
        Line_Accepted,      //Grbl returned OK
        Line_Refused        //Grbl returned an error
    };

    Q_ENUMS(Status)

    explicit GCodeJobLine(QString lineContent = QString(), quint32 m_lineNumber = 0);
    virtual ~GCodeJobLine(){}

    QString getString() const;
    QString getSimplifiedString() const;
    quint32 getLineNumber() const;
    Status getStatus() const;
    void setStatus(Status status);
    QVector<QVector3D> getPath() const;
    QVector3D getStartCoordinates() const;
    bool getIsWork() const;
    quint32 getEstimatedMachineTime() const;
    quint32 updateEstimatedMachineTime(QVector3D machineSeekRatePerAxis = QVector3D(0,0,0)); //return a value in milliseconds

    bool isCompleted() const;

    void update(GCodeParser* parser);

    bool isFlagged() const;
    void setFlagged(bool flagged);

private:
    QString m_string;
    quint32 m_lineNumber;
    QVector<QVector3D> m_pathPoints;
    bool m_isWork;
    float m_machineSpeed;
    quint32 m_estimatedMachineTime;
    Status m_status;
    GCodeJob* m_parentJob;
    bool m_isFlagged;
};

Q_DECLARE_METATYPE(QVector<QVector3D>)

#endif // GCODELINE_H
