#ifndef GCODEDEFINITIONS_H
#define GCODEDEFINITIONS_H

#define MM_PER_INCH (25.40)
#define ARC_ERROR   0.1

#define GCODE_COMMENTS_SIMPLE_BEGIN         ';'
#define GCODE_COMMENTS_DELIMITED_BEGIN      '('
#define GCODE_COMMENTS_DELIMITED_END        ')'
#define GCODE_SPACE                         ' '

#endif // GCODEDEFINITIONS_H
