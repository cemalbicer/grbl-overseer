#include "gcodejobline.h"
#include "gcodejob.h"
#include "gcodeparser.h"
#include "gcodejobmanager.h"
#include "gcodedefinitions.h"

//const char *GCodeJobLine::s_gcodeCommentsDelimiters[] = {GCODE_COMMENTS_DELIM};

GCodeJobLine::GCodeJobLine(QString lineContent, quint32 lineNumber):
    m_string(lineContent.trimmed()),
    m_lineNumber(lineNumber),
    m_isWork(false),
    m_machineSpeed(0.0f),
    m_estimatedMachineTime(0),
    m_status(Line_Ready),
    m_isFlagged(false)
{
}

QString GCodeJobLine::getString() const
{
    return m_string;
}

QString GCodeJobLine::getSimplifiedString() const
{
    QString simplifiedString = m_string;

    //Truncate simple comments
    int simpleCommentBeginIndex = simplifiedString.indexOf(GCODE_COMMENTS_SIMPLE_BEGIN);
    if(simpleCommentBeginIndex >= 0)
    {
        simplifiedString.truncate(simpleCommentBeginIndex);
    }

    //Remove delimited comments
    int delimitedCommentBeginIndex = simplifiedString.indexOf(GCODE_COMMENTS_DELIMITED_BEGIN);
    if(delimitedCommentBeginIndex >= 0)
    {
        int delimitedCommentEndIndex = simplifiedString.indexOf(GCODE_COMMENTS_DELIMITED_END);

        //If end of delimited comment not found, we assume comment goes to the end of the instruction
        int commentLength = (delimitedCommentEndIndex == -1)
                ? (simplifiedString.length() - delimitedCommentBeginIndex)
                : (delimitedCommentEndIndex + 1 - delimitedCommentBeginIndex);

        simplifiedString.remove(delimitedCommentBeginIndex, commentLength);
    }

    return simplifiedString.trimmed().toUpper();
}

/*QString GCodeJobLine::getParseableString() const
{
    QString parseableString(m_string);
    const int commentDelimiterCount = sizeof(s_gcodeCommentsDelimiters)/sizeof(s_gcodeCommentsDelimiters[0]);
    for(int i = 0 ; i < commentDelimiterCount ; i++){
        int delimiterPosition = parseableString.indexOf(s_gcodeCommentsDelimiters[i]); //Return start of comment or -1
        if(delimiterPosition >= 0){
            parseableString.truncate(delimiterPosition);     //Resize to start of comment. If -1, do nothing
        }
    }

    //remove any start / and whitespace and special character
    return parseableString.trimmed().toUpper();
}*/

quint32 GCodeJobLine::getLineNumber() const
{
    return m_lineNumber;
}

GCodeJobLine::Status GCodeJobLine::getStatus() const
{
    return m_status;
}

void GCodeJobLine::setStatus(GCodeJobLine::Status status)
{
    m_status = status;
}

QVector<QVector3D> GCodeJobLine::getPath() const
{
    return m_pathPoints;
}

QVector3D GCodeJobLine::getStartCoordinates() const
{
    return m_pathPoints.first();
}

bool GCodeJobLine::getIsWork() const
{
    return m_isWork;
}

quint32 GCodeJobLine::getEstimatedMachineTime() const //return a value in milliseconds
{
    return m_estimatedMachineTime;
}

quint32 GCodeJobLine::updateEstimatedMachineTime(QVector3D machineSeekRatePerAxis)
{
    m_estimatedMachineTime = 0;

    if(m_pathPoints.size() >= 2)    //Only proceed if at least 2 positions known (aka 1 segment)
    {
        if(m_isWork)    //If work movement, compute the sum of time for each segment
        {
            QVector<QVector3D>::const_iterator it_prevPoint = m_pathPoints.constBegin();
            QVector<QVector3D>::const_iterator it_currPoint = it_prevPoint + 1;

            while(it_currPoint != m_pathPoints.constEnd())
            {
                float dist = (*it_currPoint).distanceToPoint(*it_prevPoint);
                m_estimatedMachineTime += (dist / m_machineSpeed) * 1000.0f;  //convert to ms
                it_prevPoint = it_currPoint++;
            }
        }
        else    //Seek movement : compute time per axis, and return the largest
        {
            QVector3D movementPerAxis = m_pathPoints.last()-m_pathPoints.first();
            QVector3D machineTimePerAxis = movementPerAxis / machineSeekRatePerAxis;

            for(quint8 i = 0 ; i < 3 ; i++){
                float timeForCurrentAxis = qAbs(machineTimePerAxis[i]) *1000.0f; //convert to ms
                if(timeForCurrentAxis > m_estimatedMachineTime)
                {
                    m_estimatedMachineTime = timeForCurrentAxis;
                }
            }
        }
    }

    return m_estimatedMachineTime;
}

bool GCodeJobLine::isCompleted() const
{
    return (m_status == Line_Accepted) || (m_status == Line_Refused);
}

void GCodeJobLine::update(GCodeParser *parser)
{
    parser->processLine(getSimplifiedString());
    m_isWork = parser->getLastLineIsWork();
    m_pathPoints = parser->getLastLinePathPoints();
    m_machineSpeed = parser->getLastLineMachineRate();
}

bool GCodeJobLine::isFlagged() const
{
    return m_isFlagged;
}

void GCodeJobLine::setFlagged(bool flagged)
{
    m_isFlagged = flagged;
}
