#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <QByteArray>
#include <QList>

// Code taken from Qt 5.5.1 QRingBuffer class

class RingBuffer
{
public:
    explicit RingBuffer(int growth = 4096);

    int nextDataBlockSize() const;
    const char *readPointer() const;
    const char *readPointerAtPosition(qint64 pos, qint64 &length) const;
    void free(int bytes);
    char *reserve(int bytes);
    void truncate(int pos);
    void chop(int bytes);
    bool isEmpty() const;
    int getChar();
    void putChar(char c);
    void ungetChar(char c);
    int size() const;
    void clear();
    int indexOf(char c) const;
    int indexOf(char c, int maxLength) const;
    int read(char *data, int maxLength);
    QByteArray read();
    void append(const QByteArray &qba);
    int skip(int length);
    int readLine(char *data, int maxLength);
    bool canReadLine() const;

private:
    QList<QByteArray> m_buffers;
    int m_head;
    int m_tail;
    int m_tailBuffer; // always buffers.size() - 1
    const int m_basicBlockSize;
    int m_bufferSize;
};

#endif // RINGBUFFER_H
