#ifndef ANDROIDUSBBRIDGESERIALPORT_H
#define ANDROIDUSBBRIDGESERIALPORT_H

#include <QObject>
#include <QIODevice>

#include <QStringList>
#include "ringbuffer.h"


class AndroidUsbBridgeSerialPort : public QIODevice
{
    Q_OBJECT

public:
    AndroidUsbBridgeSerialPort();
    virtual ~AndroidUsbBridgeSerialPort() {}

    bool open(QIODevice::OpenMode);

    void close();

    void setPortName(QString portName);

    void setParameters(int baudRateA, int dataBitsA, int stopBitsA, int parityA);

    void newDataArrived(char *bytesA, int lengthA);

    static QStringList scanForAvailablePorts();

protected:

    qint64 readData(char *data, qint64 maxlen);

    qint64 writeData(const char *data, qint64 len);


private:
    void startReadThread();
    void stopReadThread();
    bool writeDataOneShot();
    qint64 writeToPort(const char *data, qint64 maxSize);

    bool isReadStopped;
    int deviceId;
    int descriptor;
    //bool hasRegisteredFunctions;
    QString systemLocation;
    qint64 pendingBytesWritten;
    qint64 readBufferMaxSize;
    qint64 internalWriteTimeoutMsec;
    RingBuffer writeBuffer;
    RingBuffer readBuffer;

};

#endif // ANDROIDUSBBRIDGESERIALPORT_H
