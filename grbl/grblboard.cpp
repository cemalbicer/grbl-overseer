#include "grblboard.h"
#include "grbldefinitions.h"
#include "grblinstruction.h"
#include "grblmessage.h"
#include "grblhistorylogic.h"
#include "grblstatuslogic.h"
#include "grblconfiglogic.h"
#include "grblbuildinfoslogic.h"
#include "grblengine.h"
#include "activities/grbljogactivity.h"
#include "activities/grbljobactivity.h"
#include "activities/grblinstqueueactivity.h"
#include <gcode/gcodejob.h>

#include <QDebug>

const QVector<QString> GrblBoard::s_startupInstructionList =
{
    QStringLiteral(GRBL_INST_GET_BUILD_INFOS),
    QStringLiteral(GRBL_INST_GET_PARAMS)
};

GrblBoard::GrblBoard(QObject *parent) :
    QObject(parent),
    m_isIdle(false),
    m_isJogAllowed(false),
    m_isSimulation(false),
    m_homingState(Homing_Invalid),
    m_engine(new GrblEngine(this)),
    m_config(new GrblConfigLogic(m_engine, this)),
    m_status(new GrblStatusLogic(m_engine, this)),
    m_history(new GrblHistoryLogic(m_engine, this)),
    m_buildInfos(new GrblBuildInfosLogic(m_engine, this))
{
    connect(m_buildInfos, &GrblBuildInfosIface::charBufferDepthChanged,
            m_engine, &GrblEngine::setBoardCharBufferDepth);

    connect(m_engine, &GrblEngine::msgReceivedTypeWelcome,
            this, &GrblBoard::onWelcomeMsgReceived);

    connect(m_engine, &GrblEngine::msgReceivedTypeError,
            this, &GrblBoard::onErrorMsgReceived);

    connect(m_engine, &GrblEngine::msgReceivedTypeAlarm,
            this, [=](const GrblMessage &response)
    {
        emit alarm(response.getText());
    });

    connect(m_engine, &GrblEngine::currentActivityChanged,
            this ,&GrblBoard::updateFlags);

    connect(m_engine, &GrblEngine::linkDeviceChanged,
            this ,&GrblBoard::updateFlags);

    connect(m_status, &GrblStatusIface::currentStateChanged,
            this, &GrblBoard::updateFlags);

    connect(m_config, &GrblConfigLogic::reportUnitIsInchChanged,
            m_status, &GrblStatusLogic::onUnitIsInchesChanged);
}

GrblStatusIface *GrblBoard::status()
{
    return static_cast<GrblStatusIface*>(m_status);
}

GrblHistoryIface *GrblBoard::history()
{
    return static_cast<GrblHistoryIface*>(m_history);
}

GrblConfigIface *GrblBoard::configuration()
{
    return static_cast<GrblConfigIface*>(m_config);
}

GrblBuildInfosIface *GrblBoard::buildInfos()
{
    return static_cast<GrblBuildInfosIface*>(m_buildInfos);
}

const GCodeJob *GrblBoard::getCurrentJob() const
{
    GrblJobActivity* currJobActivity = qobject_cast<GrblJobActivity*>(m_engine->getCurrentActivity());
    return currJobActivity ? currJobActivity->getJobPtr() : nullptr;
}

void GrblBoard::reset()
{
    m_engine->pushRealTimeCommand(GrblEngine::RtCmd_SoftReset);
}

void GrblBoard::hold()
{
    m_engine->pushRealTimeCommand(GrblEngine::RtCmd_FeedHold);
}

void GrblBoard::resume()
{
    m_engine->pushRealTimeCommand(GrblEngine::RtCmd_Resume);
}

void GrblBoard::homing()
{
    if((m_isIdle || m_status->getCurrentState() == GrblStatusLogic::State_Alarm)
            && (m_homingState != Homing_Running))
    {
        if(executeSingleInstruction(GRBL_INST_HOMING))
        {
            setHomingState(Homing_Running);
        }
    }
}

void GrblBoard::killAlarm()
{
    if(m_status->getCurrentState() == GrblStatusLogic::State_Alarm)
    {
        executeSingleInstruction(GRBL_INST_KILL_ALARM);
    }
}

void GrblBoard::toggleSpindleStop()
{
    m_engine->pushRealTimeCommand(GrblEngine::RtCmd_ToggleSpindleStop);
}

void GrblBoard::toggleCoolantMist()
{
    m_engine->pushRealTimeCommand(GrblEngine::RtCmd_ToggleMistCoolant);
}

void GrblBoard::toggleCoolantFlood()
{
    m_engine->pushRealTimeCommand(GrblEngine::RtCmd_ToggleFloodCoolant);
}

void GrblBoard::applyOverride(GrblBoard::Overrides override)
{
    static QMap<Overrides,GrblEngine::RealTimeCommand> ovrToRtCmdMap =
    {
        { Ovr_Feed_Normal       , GrblEngine::RtCmd_FeedRateNormal      },
        { Ovr_Feed_Inc          , GrblEngine::RtCmd_FeedRateInc         },
        { Ovr_Feed_Dec          , GrblEngine::RtCmd_FeedRateDec         },
        { Ovr_Feed_Inc_Fine     , GrblEngine::RtCmd_FeedRateIncFine     },
        { Ovr_Feed_Dec_Fine     , GrblEngine::RtCmd_FeedRateDecFine     },

        { Ovr_Rapid_Normal      , GrblEngine::RtCmd_RapidRateNormal     },
        { Ovr_Rapid_Slow        , GrblEngine::RtCmd_RapidRateSlow       },
        { Ovr_Rapid_Slower      , GrblEngine::RtCmd_RapidRateSlower     },

        { Ovr_Spindle_Normal    , GrblEngine::RtCmd_SpindleNormal       },
        { Ovr_Spindle_Inc       , GrblEngine::RtCmd_SpindleInc          },
        { Ovr_Spindle_Dec       , GrblEngine::RtCmd_SpindleDec          },
        { Ovr_Spindle_Inc_Fine  , GrblEngine::RtCmd_SpindleIncFine      },
        { Ovr_Spindle_Dec_Fine  , GrblEngine::RtCmd_SpindleDecFine      },
    };

    if(m_engine->isAvailable() && ovrToRtCmdMap.contains(override))
    {
        m_engine->pushRealTimeCommand(ovrToRtCmdMap.value(override));
    }
}

void GrblBoard::setLinkDevice(QIODevice *device)
{
    m_engine->setLinkDevice(device);
}

bool GrblBoard::runJob(GCodeJob *job)
{
    if(job && m_engine->isIdle())
    {
        QVector3D currMachPos = m_isSimulation
                ? m_simulationPosition
                : m_status->getMachinePositionInMm();

        GrblJobActivity *jobActivity = new GrblJobActivity(job, currMachPos, this);

        jobActivity->setHoldOnError(!m_isSimulation);

        //Store the last position of this job to set correct offset for the next
        if(m_isSimulation)
        {
            m_simulationPosition = job->getEndPosition() + job->getOrigin();
        }

        connect(jobActivity, &GrblJobActivity::jobCompleted,
                this, &GrblBoard::jobCompleted);

        m_engine->executeActivity(jobActivity);

        return true;
    }

    return false;
}

bool GrblBoard::jog(QVector3D jogVector)
{
    //If engine is not running any activity, start a new jog activity
    if(m_engine->isIdle())
    {
        GrblJogActivity *jogActivity = new GrblJogActivity(m_buildInfos->getPlanBufferDepth(), this);
        jogActivity->jog(jogVector);
        m_engine->executeActivity(jogActivity);
        return true;
    }

    //Engine might be running an activity, if a jog activity, pass it the jog value
    GrblJogActivity* currJogActivity = qobject_cast<GrblJogActivity*>(m_engine->getCurrentActivity());
    if(currJogActivity)
    {
        currJogActivity->jog(jogVector);
        return true;
    }

    return false;
}



bool GrblBoard::setSimulationMode(bool enable)
{
    const GrblStatusLogic::State &currState = m_status->getCurrentState();

    //Only allow mode switching when no commands are waiting, and current state allows it
    if(!m_engine->isIdle()
    || !(currState == GrblStatusLogic::State_Idle || currState == GrblStatusLogic::State_Check))
    {
        return false;
    }

    //Switch to simulation mode
    if((enable && currState == GrblStatusLogic::State_Idle)
    ||(!enable && currState == GrblStatusLogic::State_Check))
    {
        return executeSingleInstruction(GRBL_INST_TOGGLE_CHECK);
    }

    //Nothing to do
    return true;
}
void GrblBoard::onWelcomeMsgReceived(const GrblMessage &message)
{
    //Send startup instruction sequence as soon as we reach event loop (10ms delay to receive messages before)
    QTimer::singleShot(0,this, &GrblBoard::executeStartupInstructions);

    //If board is not supported (won't be able to get status), signal it
    if(message.getText().startsWith(QStringLiteral(OBSOLETE_VERSION_STRING)))
    {
        emit boardVersionNotSupported();
    }

    //setHomingState(Homing_Invalid);
}

void GrblBoard::onErrorMsgReceived(const GrblMessage &message)
{
    QString errorMsg = message.getText();

    //If error encountered while running a job, add infos on job errors
    const GCodeJob * currentJob = getCurrentJob();
    if(currentJob)
    {
        // If job and not check state,
        // send feed hold command to avoid any damage
        /*if(m_status->getCurrentState() != GrblStatusLogic::State_Check)
        {
            m_engine->pushRealTimeCommand(GrblEngine::RtCmd_FeedHold);
            m_engine->pushRealTimeCommand(GrblEngine::RtCmd_StatusRequest);
        }*/


        QString jobErrorInfoString = QString("Job %1 line %2 :");
        QString jobName = currentJob->getName();


        GrblInstructionPointer relatedInstruction = message.getRelatedInstruction();

        QString jobLine = relatedInstruction
                ? QString::number(relatedInstruction->getLineNumber())
                : QString("unknown");

        errorMsg.prepend(jobErrorInfoString.arg(jobName).arg(jobLine));
    }

    emit error(errorMsg);
}

bool GrblBoard::executeSingleInstruction(QString instructionString)
{
    // Create an activity with the user instruction, and a BARRIER instruction
    // This way activity is only completed after all instruction are effectively completed
    GrblInstQueueActivity *singleInstActivity = new GrblInstQueueActivity({instructionString},this);
    return m_engine->executeActivity(singleInstActivity);
}

bool GrblBoard::executeStartupInstructions()
{
    GrblInstQueueActivity *startupActivity = new GrblInstQueueActivity(s_startupInstructionList,this);
    connect(startupActivity, &GrblInstQueueActivity::completed,
            this, &GrblBoard::onStartupActivityCompleted);
    return m_engine->executeActivity(startupActivity);
}

void GrblBoard::setHomingState(GrblBoard::HomingState state)
{
    if(state != m_homingState)
    {
        m_homingState = state;
        emit homingStateChanged(m_homingState);
    }
}

void GrblBoard::setIsSimulation(bool simulation)
{
    if(simulation != m_isSimulation)
    {
        m_isSimulation = simulation;
        m_simulationPosition = simulation
                ? m_status->getMachinePositionInMm()
                : QVector3D();
        emit isSimulationModeChanged();
    }
}

void GrblBoard::onStartupActivityCompleted()
{
    //emit boardResetSequenceCompleted();

    //qDebug() << "Startup activity completed";

    //only way to leave simulation mode is by resetting the board
    if(m_isSimulation && m_status->getCurrentState() != GrblStatusLogic::State_Check)
    {
        setIsSimulation(false);
    }
}

void GrblBoard::updateFlags()
{
    GrblStatusIface::State currentState = m_status->getCurrentState();

    //Board idle if engine idle and status idle
    bool isIdleNow = m_engine->isIdle() &&
            (currentState == GrblStatusIface::State_Idle);

    //Jog allowed if board idle or current activity is jog
    bool isJogAllowedNow = isIdleNow ||
            (qobject_cast<GrblJogActivity*>(m_engine->getCurrentActivity()));

    if(m_isIdle != isIdleNow)
    {
        m_isIdle = isIdleNow;
        emit isIdleChanged();
    }

    if(m_isJogAllowed != isJogAllowedNow)
    {
        m_isJogAllowed = isJogAllowedNow;
        emit isJogAllowedChanged();
    }

    //If state changed TO simulation (and only TO).
    // The other way (reverting from simulation) is done in onResetCompleted
    if(!m_isSimulation &&
            currentState == GrblStatusLogic::State_Check &&
            m_engine->getCurrentActivity() == nullptr)
    {
        setIsSimulation(true);
    }

    //Homing is lost when switching to alarm mode
    if(m_homingState != Homing_Invalid &&
            currentState == GrblStatusIface::State_Alarm)
    {
        setHomingState(Homing_Invalid);
    }

    //Else homing is completed when state is known again
    else if(m_homingState == Homing_Running &&
            currentState != GrblStatusIface::State_Unknown)
    {
        setHomingState(Homing_Valid);
    }
}

