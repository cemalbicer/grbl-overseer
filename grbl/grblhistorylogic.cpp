#include "grblhistorylogic.h"
#include "grblengine.h"
#include "grblboard.h"

#define GRBLBOARD_DEFAULT_HISTORY_LENGTH          100


GrblHistoryLogic::HistoryItem::HistoryItem()
{}

GrblHistoryLogic::HistoryItem::HistoryItem(GrblInstructionPointer instruction) :
    instruction(instruction)
{}

GrblHistoryLogic::HistoryItem::HistoryItem(GrblMessage message) :
    messages({message})
{}

//##############################################################################

const QMap<GrblInstruction::Status,GrblHistoryLogic::Status> GrblHistoryLogic::s_instructionToItemStatusMap =
        QMap<GrblInstruction::Status,GrblHistoryLogic::Status>(
{
            {GrblInstruction::Status_Ready  , GrblHistoryLogic::Status_None      },
            {GrblInstruction::Status_Sent   , GrblHistoryLogic::Status_Sent      },
            {GrblInstruction::Status_Ok     , GrblHistoryLogic::Status_Ok        },
            {GrblInstruction::Status_Error  , GrblHistoryLogic::Status_Error     },
            {GrblInstruction::Status_Aborted, GrblHistoryLogic::Status_Aborted   },
        });


GrblHistoryLogic::GrblHistoryLogic(GrblEngine *engine, GrblBoard *parent) :
    GrblHistoryIface(parent),
    m_capacity(GRBLBOARD_DEFAULT_HISTORY_LENGTH)
{
    m_history.reserve(m_capacity);

    connect(engine, &GrblEngine::instructionSent,
            this, &GrblHistoryLogic::onInstructionSent);

    connect(engine, &GrblEngine::instructionCompleted,
            this, &GrblHistoryLogic::onInstructionCompleted);

    //Receive all messages except status messages (we do not want to display them !)
    connect(engine, &GrblEngine::msgReceivedTypeOk,
            this, &GrblHistoryLogic::onMessageReceived);

    connect(engine, &GrblEngine::msgReceivedTypeError,
            this, &GrblHistoryLogic::onMessageReceived);

    connect(engine, &GrblEngine::msgReceivedTypeAlarm,
            this, &GrblHistoryLogic::onMessageReceived);

    connect(engine, &GrblEngine::msgReceivedTypeFeedback,
            this, &GrblHistoryLogic::onMessageReceived);

    connect(engine, &GrblEngine::msgReceivedTypeWelcome,
            this, &GrblHistoryLogic::onMessageReceived);

    connect(engine, &GrblEngine::msgReceivedTypeParameter,
            this, &GrblHistoryLogic::onMessageReceived);

    connect(engine, &GrblEngine::msgReceivedTypeUnknown,
            this, &GrblHistoryLogic::onMessageReceived);
}

void GrblHistoryLogic::onInstructionSent(GrblInstructionPointer instPtr)
{
    addItemToHistory(HistoryItem(instPtr));
}

void GrblHistoryLogic::onInstructionCompleted(GrblInstructionPointer instPtr)
{
    //Locate if we have this instruction in history
    int itemIndex = findItemFromInstruction(instPtr);

    //If we do, only notify about data changed,
    // status is already updated in instruction (because it's shared)
    if(itemIndex >= 0)
    {
        QModelIndex changedIndex = index(itemIndex);
        emit dataChanged(changedIndex, changedIndex,
                         QVector<int>({StatusRole}));
    }
}

void GrblHistoryLogic::onMessageReceived(GrblMessage message)
{
    GrblInstructionPointer relatedInstPtr = message.getRelatedInstruction();

    //Messages not related to any instruction are added as a new item
    if(relatedInstPtr.isNull())
    {
        addItemToHistory(HistoryItem(message));
        emit messageAddedToItem(m_history.size() - 1);
    }

    //Messages related to an instruction are added to that instruction
    else
    {
        int itemIndex = findItemFromInstruction(relatedInstPtr);

        if(itemIndex >= 0)
        {
            m_history[itemIndex].messages.append(message);

            QModelIndex changedIndex = index(itemIndex);
            emit dataChanged(changedIndex, changedIndex, {SubTextsRole});
        }

        emit messageAddedToItem(itemIndex);
    }
}
void GrblHistoryLogic::addItemToHistory(GrblHistoryLogic::HistoryItem item)
{
    while(m_history.size() >= m_capacity)
    {
        beginRemoveRows(QModelIndex(), 0, 0 );
        m_history.removeFirst();
        endRemoveRows();
    }

    beginInsertRows(QModelIndex(), m_history.size(), m_history.size());
    m_history.append(item);
    endInsertRows();
}

int GrblHistoryLogic::findItemFromInstruction(GrblInstructionPointer instPtr)
{
    for(int i = 0 ; i < m_history.size() ; i++)
    {
        if(m_history.at(i).instruction == instPtr)
        {
            return i;
        }
    }

    return -1;
}


int GrblHistoryLogic::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_history.size();
}

QVariant GrblHistoryLogic::data(const QModelIndex &index, int role) const
{
    int itemIndex = index.row();

    if(index.parent().isValid()
            || index.column() != 0
            || itemIndex >= m_history.size())
    {
        return QVariant();
    }

    const HistoryItem& item = m_history.at(itemIndex);

    switch(role)
    {

    //Contains (QString) instruction if not null, else first message
    case MainTextRole:
    {
        return item.instruction.isNull()
                ? item.messages.at(0).getText()
                : item.instruction->getStringWithLineNumber();
    }

    //Contains messages (QStringList) only if instruction is not null.
    case SubTextsRole:
    {
        QStringList subTextsList;
        if(!item.instruction.isNull()){
            for(QVector<GrblMessage>::const_iterator i = item.messages.cbegin() ;
                i != item.messages.cend() ;
                i++)
            {
                subTextsList.append(i->getText());
            }
        }
        return subTextsList;
    }

        //Contains (Status) instruction status if not null, else first message type (Alarm or not)
    case StatusRole:
    {
        if(item.instruction.isNull())
        {
            return (item.messages.at(0).getType() == GrblMessage::Type_Alarm) ? Status_Alarm : Status_None;
        }
        else
        {
            GrblInstruction::Status instStatus = item.instruction->getStatus();
            Status statusToReturn = s_instructionToItemStatusMap.value(instStatus, GrblHistoryLogic::Status_None);
            return statusToReturn;
        }
    }

    default:
        return QVariant();
    }
}

QHash<int, QByteArray> GrblHistoryLogic::roleNames() const
{
    return QHash<int, QByteArray>(
    {
        {StatusRole,            "status"               },
        {MainTextRole,          "mainLine"             },
        {SubTextsRole,          "subLines"             },
    });
}

