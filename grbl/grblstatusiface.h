#ifndef GRBLSTATUSIFACE_H
#define GRBLSTATUSIFACE_H

#include <QObject>
#include <QVector3D>

#define STATUS_DEFAULT_REQUEST_INTERVAL 200


class GrblStatusIface : public QObject
{
    Q_OBJECT

    Q_PROPERTY(State currentState READ getCurrentState NOTIFY currentStateChanged)

    // converted position
    Q_PROPERTY(QVector3D machinePositionMm          READ getMachinePositionInMm         NOTIFY machinePositionChanged)
    Q_PROPERTY(QVector3D workPositionMm             READ getWorkPositionInMm            NOTIFY workPositionChanged)

    // raw position
    Q_PROPERTY(QVector3D rawPosition            READ getRawPosition                 NOTIFY rawPositionChanged)
    Q_PROPERTY(bool isRawPositionInInches       READ getIsRawPositionInInches       NOTIFY isRawPositionInInchesChanged)
    Q_PROPERTY(bool isRawPositionInWorkCoord    READ getIsRawPositionInWorkCoord    NOTIFY isRawPositionInWorkCoordChanged)

    // overrides
    Q_PROPERTY(int overrideFeed     READ getOverrideFeed    NOTIFY overrideFeedChanged)
    Q_PROPERTY(int overrideRapid    READ getOverrideRapid   NOTIFY overrideRapidChanged)
    Q_PROPERTY(int overrideSpindle  READ getOverrideSpindle NOTIFY overrideSpindleChanged)

    // IOs
    Q_PROPERTY(InputPins activeInputPins READ getActiveInputPins NOTIFY activeInputPinsChanged)
    Q_PROPERTY(AccessoryFlags accessoryState READ getAccessoryState NOTIFY accessoryStateChanged)

    // Buffers
    Q_PROPERTY(bool hasBufferStatus     READ getHasBufferStatus     NOTIFY hasBufferStatusChanged)
    Q_PROPERTY(int planBufferFreeSlots  READ getPlanBufferFreeSlots NOTIFY planBufferFreeSlotsChanged)
    Q_PROPERTY(int charBufferFreeSlots  READ getCharBufferFreeSlots NOTIFY charBufferFreeSlotsChanged)


public:

    enum State
    {
        State_Idle, State_Run, State_Hold, State_Jog, State_Door,
        State_Home, State_Alarm, State_Check, State_Sleep,
        State_Unknown, State_Offline
    };

    Q_ENUMS(State)

    enum InputPin
    {
        Input_X_Limit           = 1<<0,
        Input_Y_Limit           = 1<<1,
        Input_Z_Limit           = 1<<2,

        Input_Probe             = 1<<3,

        Input_Door              = 1<<4,
        Input_Hold              = 1<<5,
        Input_SoftReset         = 1<<6,
        Input_CycleStart        = 1<<7,
    };

    Q_DECLARE_FLAGS(InputPins, InputPin)
    Q_FLAG(InputPins)

    Q_ENUMS(InputPin)

    enum AccessoryFlag
    {
        Accessory_Spindle_CW    = 1<<0,
        Accessory_Spindle_CCW   = 1<<1,

        Accessory_Coolant_Flood = 1<<2,
        Accessory_Coolant_Mist  = 1<<3
    };

    Q_ENUMS(AccessoryFlag)

    Q_DECLARE_FLAGS(AccessoryFlags, AccessoryFlag)
    Q_FLAG(AccessoryFlags)

    explicit GrblStatusIface(QObject *parent = 0) : QObject(parent) {}

    virtual State getCurrentState() const = 0;
    virtual State getPreviousState() const = 0;

    virtual QVector3D getMachinePositionInMm() const = 0;
    virtual QVector3D getWorkPositionInMm() const = 0;

    virtual QVector3D getRawPosition() const = 0;   //Directly returns position reported by GRBL (no conversion)
    virtual bool getIsRawPositionInInches() const = 0;
    virtual bool getIsRawPositionInWorkCoord() const = 0;

    virtual InputPins getActiveInputPins() const = 0;

    virtual AccessoryFlags getAccessoryState() const = 0;

    virtual bool getHasBufferStatus() const = 0;
    virtual int getPlanBufferFreeSlots() const = 0;
    virtual int getCharBufferFreeSlots() const = 0;

    virtual int getOverrideFeed() const = 0;        // in % of nominal speed
    virtual int getOverrideRapid() const = 0;       // in % of nominal speed
    virtual int getOverrideSpindle() const = 0;     // in % of nominal speed

signals:
    void updated();

    void currentStateChanged(State state);

    void machinePositionChanged(QVector3D machinePosition);
    void workPositionChanged(QVector3D workPosition);

    void rawPositionChanged(QVector3D rawPosition);
    void isRawPositionInInchesChanged(bool isInches);
    void isRawPositionInWorkCoordChanged(bool isWorkCoord);

    void overrideFeedChanged(int overrideFeed);
    void overrideRapidChanged(int overrideRapid);
    void overrideSpindleChanged(int overrideSpindle);

    void activeInputPinsChanged(InputPins activeInputPins);

    void accessoryStateChanged(AccessoryFlags accessoryState);

    void hasBufferStatusChanged(bool hasBufferStatus);
    void planBufferFreeSlotsChanged(int freeSlotsCount);
    void charBufferFreeSlotsChanged(int freeSlotsCount);

public slots:
    virtual void refresh() = 0;
};

#endif // GRBLSTATUSIFACE_H
