#ifndef GRBLSTRINGREGISTER_H
#define GRBLSTRINGREGISTER_H

#include <QSettings>

class GrblStringRegister
{
public:
    enum SettingsTextRole {Setting_Label, Setting_Unit, Setting_Bits, Setting_Description};

    GrblStringRegister()    = delete;   //Not implemented
    ~GrblStringRegister()   = delete;   //Not implemented

    static void loadStringsFile(QString path);

    static bool isStringsAvailable();

    static QString getErrorString(quint8 errorId);

    static QString getAlarmString(quint8 alarmId);

    static QString getBuildOptionString(unsigned char buildOptionChar);

    static QString getSettingsString(quint8 settingKey, SettingsTextRole role);

private:
    static QSettings* s_stringFile;
};

#endif // GRBLSTRINGREGISTER_H
