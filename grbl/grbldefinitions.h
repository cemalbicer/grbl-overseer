#ifndef GRBLDEFINITIONS_H
#define GRBLDEFINITIONS_H

#define MM_PER_INCH                     (25.40)

#define GRBL_DEFAULT_CHAR_BUFFER_SIZE   127
#define GRBL_DEFAULT_PLANNER_SIZE       15
#define GRBL_EXEC_TIME_MS               10

#define LINE_SEPARATOR_STRING           "\r\n"
#define LINE_SEPARATOR_LENGTH           2

#define RESPONSE_OK                     "ok"
#define RESPONSE_ERROR                  "error:"
#define RESPONSE_ALARM                  "ALARM:"
#define RESPONSE_STATUS_START           "<"
#define RESPONSE_STATUS_END             ">"
#define RESPONSE_STATUS_DELIM           ","
#define RESPONSE_FEEDBACK_START         "["
#define RESPONSE_FEEDBACK_END           "]"
#define RESPONSE_PARAM_START            "$"

#define CMD_FEEDHOLD_CHAR               '!'
#define CMD_RESUME_CHAR                 '~'
#define CMD_STATUS_REQ_CHAR             '?'
#define CMD_SOFT_RESET_CHAR             0x18
#define CMD_SAFETY_DOOR_CHAR            0x84
#define CMD_JOG_CANCEL                  0x85
#define CMD_FEEDRATE_NORMAL             0x90
#define CMD_FEEDRATE_INC                0x91
#define CMD_FEEDRATE_DEC                0x92
#define CMD_FEEDRATE_INC_FINE           0x93
#define CMD_FEEDRATE_DEC_FINE           0x94
#define CMD_RAPIDRATE_NORMAL            0x95
#define CMD_RAPIDRATE_HALF              0x96
#define CMD_RAPIDRATE_QUARTER           0x97
#define CMD_SPINDLE_NORMAL              0x99
#define CMD_SPINDLE_INC                 0x9A
#define CMD_SPINDLE_DEC                 0x9B
#define CMD_SPINDLE_INC_FINE            0x9C
#define CMD_SPINDLE_DEC_FINE            0x9D
#define CMD_TOGGLE_SPINDLE_STOP         0x9E
#define CMD_TOGGLE_FLOOD_COOL           0xA0
#define CMD_TOGGLE_MIST_COOL            0xA1

#define STATE_IDLE_STRING               "Idle"
#define STATE_RUN_STRING                "Run"
#define STATE_HOLD_STRING               "Hold"
#define STATE_JOG_STRING                "Jog"
#define STATE_ALARM_STRING              "Alarm"
#define STATE_DOOR_STRING               "Door"
#define STATE_CHECK_STRING              "Check"
#define STATE_HOME_STRING               "Home"
#define STATE_SLEEP_STRING              "Sleep"

#define STATUS_FIELD_SEPARATOR          '|'
#define STATUS_MACHINE_POS              "MPos:"
#define STATUS_WORK_POS                 "WPos:"
#define STATUS_WORK_COORD_OFFSET        "WCO:"
#define STATUS_BUFFERS                  "Bf:"
#define STATUS_OVERRIDES                "Ov:"
#define STATUS_ACTIVE_INPUT_PINS        "Pn:"
#define STATUS_ACCESSORY_STATE          "A:"
#define STATUS_BUFFER_SEPARATOR         ','

#define INPUT_PIN_X_LIMIT               'X'
#define INPUT_PIN_Y_LIMIT               'Y'
#define INPUT_PIN_Z_LIMIT               'Z'
#define INPUT_PIN_PROBE                 'P'
#define INPUT_PIN_DOOR                  'D'
#define INPUT_PIN_HOLD                  'H'
#define INPUT_PIN_SOFT_RESET            'R'
#define INPUT_PIN_CYCLE_START           'S'

#define ACCESSORY_SPINDLE_CW            'S'
#define ACCESSORY_SPINDLE_CCW           'C'
#define ACCESSORY_COOLANT_FLOOD         'F'
#define ACCESSORY_COOLANT_MIST          'M'

#define GRBL_WELCOME_STRING             "Grbl "
#define OBSOLETE_VERSION_STRING         "Grbl 0"

#define GRBL_BUILD_VERSION_STRING       "VER:"
#define GRBL_BUILD_OPTIONS_STRING       "OPT:"
#define GRBL_BUILD_OPTIONS_SEPARATOR    ','

#define GRBL_OPTION_VAR_SPINDLE         'V'
#define GRBL_OPTION_LINE_NUMBERS        'N'
#define GRBL_OPTION_MIST_COOLANT        'M'
#define GRBL_OPTION_COREXY              'C'
#define GRBL_OPTION_PARKING_MOTION      'P'
#define GRBL_OPTION_HOMING_FORCE_ORIGIN 'Z'
#define GRBL_OPTION_HOMING_SINGLE_AXIS  'H'
#define GRBL_OPTION_2_LIM_SWITCHES_AXIS 'T'
#define GRBL_OPTION_SPNDL_DIR_PIN_AS_EN 'D'
#define GRBL_OPTION_OFF_WHEN_SPEED_0    '0'
#define GRBL_OPTION_SOFT_LIMIT_DEBOUNCE 'S'
#define GRBL_OPTION_PARKING_OVR         'R'
#define GRBL_OPTION_FEED_OVR_IN_PROBING 'A'
#define GRBL_OPTION_NO_RSTRE_ALL_EEPROM '*'
#define GRBL_OPTION_NO_RSTRE_$_SETTINGS '$'
#define GRBL_OPTION_NO_RSTRE_PARAM_DATA '#'
#define GRBL_OPTION_NO_BUILD_INFO_WRITE 'I'
#define GRBL_OPTION_NO_SYNC_ON_EEPROM_W 'E'
#define GRBL_OPTION_NO_SYNC_ON_WCO_CHNG 'W'
#define GRBL_OPTION_NO_HOMING_AUTO_LOCK 'L'

#define GRBL_INST_STARTS_WITH           "$"
#define GRBL_INST_JOG                   "$J="
#define GRBL_INST_GET_PARAMS            "$$"
#define GRBL_INST_GET_BUILD_INFOS       "$I"
#define GRBL_INST_TOGGLE_CHECK          "$C"
#define GRBL_INST_KILL_ALARM            "$X"
#define GRBL_INST_HOMING                "$H"
#define GRBL_INST_PROGRAM_END           "M2"
#define GRBL_INST_BARRIER               "G4P0"

//Those are command requiring simple question / answer protocol rather than buffered protocol
//For simplification, any GRBL command is considered EEPROM related and so requiring the simpler protocol
#define INSTRUCTIONS_BLOCKING           GRBL_INST_STARTS_WITH,"G10 L2","G10 L20","G28","G30","G54","G55","G56","G57","G58","G59"

#define END_OF_INSTRUCTION              '\n'

#define GRBL_PARAM_REPORT_INCHES        13
#define GRBL_PARAM_HOMING_ENABLED       22
#define GRBL_PARAM_X_MAX_RATE           110
#define GRBL_PARAM_Y_MAX_RATE           111
#define GRBL_PARAM_Z_MAX_RATE           112
#define GRBL_PARAM_X_MAX_ACC            120
#define GRBL_PARAM_Y_MAX_ACC            121
#define GRBL_PARAM_Z_MAX_ACC            122
#define GRBL_PARAM_X_MAX_TRAVEL         130
#define GRBL_PARAM_Y_MAX_TRAVEL         131
#define GRBL_PARAM_Z_MAX_TRAVEL         132


#endif // GRBLDEFINITIONS_H
