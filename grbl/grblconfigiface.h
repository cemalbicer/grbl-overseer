#ifndef GRBLCONFIGIFACE_H
#define GRBLCONFIGIFACE_H

#include <QString>
#include <QVariant>
#include <QAbstractListModel>
#include <QVector3D>

class GrblConfigIface : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool reportUnitIsInch READ getReportUnitIsInch NOTIFY reportUnitIsInchChanged)
    Q_PROPERTY(QVector3D seekRates READ getSeekRates NOTIFY seekRatesChanged)           //in mm/s
    Q_PROPERTY(QVector3D maxAccs READ getMaxAccs NOTIFY maxAccsChanged)                 //in mm/s/s
    Q_PROPERTY(bool homingEnabled READ getHomingEnabled NOTIFY homingEnabledChanged)
    Q_PROPERTY(QVector3D machineSpace READ getMachineSpace NOTIFY machineSpaceChanged)  // in mm

public:
    enum ItemRoles {
        KeyRole = Qt::UserRole + 1,
        ValueRole,
        LabelRole,
        UnitRole,
        BitsRole,
        DescriptionRole
    };

    explicit GrblConfigIface(QObject *parent = 0) : QAbstractListModel(parent) {}

    virtual bool getReportUnitIsInch() = 0;
    virtual QVector3D getSeekRates() = 0;
    virtual QVector3D getMaxAccs() = 0;
    virtual bool getHomingEnabled() = 0;
    virtual QVector3D getMachineSpace() = 0;

signals:
    void reportUnitIsInchChanged(bool isInch);
    void seekRatesChanged(QVector3D seekRates);
    void maxAccsChanged(QVector3D maxAccs);
    void homingEnabledChanged(bool homingEnabled);
    void machineSpaceChanged(QVector3D machineSpace);

public: // QAbstractItemModel interface
    virtual int rowCount(const QModelIndex &parent) const override = 0;
    virtual QVariant data(const QModelIndex &index, int role) const override = 0;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override = 0;
};

#endif // GRBLCONFIGIFACE_H
