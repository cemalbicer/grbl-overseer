#ifndef COMMANDQUEUEACTIVITY_H
#define COMMANDQUEUEACTIVITY_H

#include "grblabstractactivity.h"

#include <QVector>

class GrblInstQueueActivity : public GrblAbstractActivity
{
    Q_OBJECT
public:
    GrblInstQueueActivity(QVector<QString> instString, GrblBoard *parent);

    void onInstructionSent(GrblInstructionPointer) override;
    void onCompleted() override;

    virtual GrblInstructionPointer getNextInstruction() override;

signals:
    void completed();

private:

    int m_currentIndex;
    QVector<GrblInstructionPointer> m_instructions;
};

#endif // COMMANDQUEUEACTIVITY_H
