#ifndef GRBLJOGACTIVITY_H
#define GRBLJOGACTIVITY_H

#include "grblabstractactivity.h"

#include <QVector3D>


class GrblBoard;

class GrblJogActivity : public GrblAbstractActivity
{
    Q_OBJECT
public:
    explicit GrblJogActivity(int plannerBufferDepth, GrblBoard *parentBoard);

    virtual GrblInstructionPointer getNextInstruction() override;

public slots:
    void jog(QVector3D jogVector);  //jobVector is expressed as fraction of seek velocity per axis

private:
    QVector3D m_jogVector;  //Job vector (set to null to stop jog)

    float m_accelMultiplier;
};

#endif // GRBLJOGACTIVITY_H
