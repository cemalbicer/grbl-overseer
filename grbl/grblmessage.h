#ifndef GRBLRESPONSE_H
#define GRBLRESPONSE_H

#include "grblinstruction.h"

#include <QString>
#include <QList>
#include <QMap>

class GrblMessage
{
public:
    enum Type{Type_Ok, Type_Error, Type_Alarm,
              Type_Status, Type_Feedback, Type_Welcome,
              Type_Parameter, Type_Unknown};

    explicit GrblMessage(QString responseString = QString());

    Type getType() const
        {return m_type;}

    QString getText() const
        {return m_text;}

    void setRelatedInstruction(GrblInstructionPointer instruction);

    const GrblInstructionPointer getRelatedInstruction() const
        {return m_instructionPtr;}

    bool isInstructionResponse() const;

private:
    QString convertNumericTextToString(quint8 value);

    Type m_type;
    QString m_text;

    GrblInstructionPointer m_instructionPtr;

    struct TypeInfoStruct
    {
        enum TextType {RawText, ExtractText, ExtractNumeric};
        QString prefix;     //How this type of response starts
        QString suffix;     //How this type of response ends
        TextType textType;  //Does text must contains prefix and suffix
        QString messageText;//How will message be formatted for user
    };

    static const QMap <Type, TypeInfoStruct> s_typeDelimiterMap;
};

#endif // GRBLRESPONSE_H
