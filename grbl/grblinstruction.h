#ifndef GRBLINSTRUCTION_H
#define GRBLINSTRUCTION_H

#include <QString>
#include <QSharedPointer>

class GrblInstruction
{
public:
    enum Status{Status_Ready, Status_Sent, Status_Ok, Status_Error, Status_Aborted};

    GrblInstruction(QString instruction, int lineNumber =-1);

    void forceBlocking();

    QByteArray getBytes() const
        {return m_instructionBytes;}

    int getLength() const
        {return m_instructionBytes.length();}

    int getLineNumber() const
        {return m_lineNumber;}

    Status getStatus() const
        {return m_status;}

    void setStatus(Status status)
        {m_status = status;}

    bool isBlocking() const
        {return m_isBlocking;}

    bool isGrblSpecific() const;

    bool isConfigurationSet() const;

    bool isConfigurationFetch() const;

    bool isProgramEnd() const;

    bool isJog() const;

    QString getString() const
        {return QString::fromLatin1(m_instructionBytes);}

    QString getStringWithLineNumber();

private:
    bool isInherentlyBlocking();
    bool compareIgnoreSpace(const char* charArray) const;
//    QByteArray getSimplifiedBytes() const;

    Status m_status;

    QByteArray m_instructionBytes;
    int m_lineNumber;
    bool m_isBlocking;

    static const char *s_blockingInstructionsList[]; //List of EEPROM related instructions, requiring use of simpler "blocking" protocol
};


typedef QSharedPointer<GrblInstruction> GrblInstructionPointer;

#endif // GRBLINSTRUCTION_H
