#ifndef GRBLCHARBUFFER_H
#define GRBLCHARBUFFER_H

#include <QObject>
#include <QList>
#include <QPointer>

#include "grblmessage.h"
#include "grblinstruction.h"

class QTimer;
class QIODevice;
class GrblAbstractActivity;


class GrblEngine  : public QObject
{
    Q_OBJECT

    Q_PROPERTY(GrblAbstractActivity* currentActivity READ getCurrentActivity WRITE executeActivity NOTIFY currentActivityChanged)
public:
    enum RealTimeCommand{
        RtCmd_FeedHold,
        RtCmd_Resume,
        RtCmd_StatusRequest,
        RtCmd_SoftReset,
        RtCmd_SafetyDoor,
        RtCmd_JogCancel,

        RtCmd_FeedRateNormal,
        RtCmd_FeedRateInc,
        RtCmd_FeedRateDec,
        RtCmd_FeedRateIncFine,
        RtCmd_FeedRateDecFine,

        RtCmd_RapidRateNormal,
        RtCmd_RapidRateSlow,
        RtCmd_RapidRateSlower,

        RtCmd_SpindleNormal,
        RtCmd_SpindleInc,
        RtCmd_SpindleDec,
        RtCmd_SpindleIncFine,
        RtCmd_SpindleDecFine,

        RtCmd_ToggleSpindleStop,
        RtCmd_ToggleFloodCoolant,
        RtCmd_ToggleMistCoolant
    };

    explicit GrblEngine(QObject *parent = 0);

    bool isOpened();
    bool isAvailable();

    bool isIdle();

    GrblAbstractActivity *getCurrentActivity();

public slots:
    void setLinkDevice(QIODevice *device);

    bool pushRealTimeCommand(RealTimeCommand command);

    bool executeActivity(GrblAbstractActivity* activityToExecute);

    void setBoardCharBufferDepth(int depth);

signals:
    void instructionSent(GrblInstructionPointer instPtr);

    void instructionCompleted(GrblInstructionPointer instPtr);

    void msgReceivedTypeOk(const GrblMessage &response);
    void msgReceivedTypeError(const GrblMessage &response);
    void msgReceivedTypeAlarm(const GrblMessage &response);
    void msgReceivedTypeStatus(const GrblMessage &response);
    void msgReceivedTypeFeedback(const GrblMessage &response);
    void msgReceivedTypeWelcome(const GrblMessage &response);
    void msgReceivedTypeParameter(const GrblMessage &response);
    void msgReceivedTypeUnknown(const GrblMessage &response);

    void linkDeviceChanged();

    void currentActivityChanged(GrblAbstractActivity* currentActivity);

private slots:
    void parseRxBuffer(void);

private:
    void abortAllInstructions();
    void abortJogInstructions();

    int getAvailableSpaceInCharBuffer();
    bool isBlockingInstructionInBuffer();

    void pushActivityInstructions();

    void emitMessageReceivedSignal(const GrblMessage &message);

    QByteArray m_rxBuffer;
    QIODevice* m_linkDevice;

    QPointer<GrblAbstractActivity> m_currentActivity;
    QList<GrblInstructionPointer> m_grblCharBuffer;
    int m_grblCharBufferDepth;

    QTimer* m_manualResetTimer;

    bool m_resetInProgress; //Connection established but board has not reset yet

    static const QMap<RealTimeCommand, char> s_rtCmdCharMap;
};

#endif // GRBLCHARBUFFER_H
