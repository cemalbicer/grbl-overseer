#ifndef GRBLBUILDINFOSLOGIC_H
#define GRBLBUILDINFOSLOGIC_H

#include "grblbuildinfosiface.h"
#include "grblmessage.h"

class GrblEngine;
class GrblBoard;

class GrblBuildInfosLogic : public GrblBuildInfosIface
{
    Q_OBJECT
public:
    explicit GrblBuildInfosLogic(GrblEngine *engine, GrblBoard *parentBoard);

    QString getBuildString() const override
        {return m_buildString;}

    int getPlanBufferDepth() const override
        {return m_planBufferDepth;}

    int getCharBufferDepth() const override
        {return m_charBufferDepth;}

    BuildOptions getOptions() const override
        {return m_buildOptions;}

private slots:
    void onFeedbackMessageReceived(GrblMessage message);

private:
    bool parseOptionsField(const QStringRef &field);
    bool parseBufferField(const QVector<QStringRef> &optionFields, int index, int* value_dest);

    QString m_buildString;
    BuildOptions m_buildOptions;
    int m_planBufferDepth;
    int m_charBufferDepth;

    static const QString s_buildVersionString;
    static const QString s_buildOptionsString;
    static const QMap<QChar, BuildOption> s_buildOptionTranslator;
};

#endif // GRBLBUILDINFOSLOGIC_H
