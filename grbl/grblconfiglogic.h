#ifndef GRBLCONFIGLOGIC_H
#define GRBLCONFIGLOGIC_H

#include "grblconfigiface.h"
#include "grblmessage.h"
#include "grblinstruction.h"

#include <QString>
#include <QVariant>
#include <QAbstractListModel>
#include <QMap>
#include <QVector3D>

class GrblBoard;
class GrblEngine;

class GrblConfigLogic : public GrblConfigIface
{
    Q_OBJECT

public:

    explicit GrblConfigLogic(GrblEngine *engine, GrblBoard *parentBoard);

    bool getReportUnitIsInch() override;
    QVector3D getSeekRates() override;
    QVector3D getMaxAccs() override;
    bool getHomingEnabled() override;
    QVector3D getMachineSpace() override;

public: // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

protected:
    QHash<int, QByteArray> roleNames() const override;

    int getKeyFromIndex(int index) const;   //returns -1 if no matching key found
    int getIndexFromKey(int key) const;     //returns index where we can insert key

private slots:
    void onParameterMessageReceived(GrblMessage message);
    void onPotentiallyConfigInstructionCompleted(GrblInstructionPointer instPtr);
    void onPotentiallyConfigInstructionSent(GrblInstructionPointer instPtr);
    void clear();

private:
    int parseConfigurationLine(QString line);
    void notifyOfPropertyChanges(int key);

    QMap<int, double> m_parametersMap;

    static const QRegularExpression s_paramRegExp;
};

#endif // GRBLCONFIGLOGIC_H
