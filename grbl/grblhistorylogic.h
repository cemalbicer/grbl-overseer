#ifndef GRBLHISTORYLOGIC_H
#define GRBLHISTORYLOGIC_H

#include "grblhistoryiface.h"
#include "grblinstruction.h"
#include "grblmessage.h"

#include <QAbstractListModel>
#include <QList>

class GrblEngine;

class GrblHistoryLogic : public GrblHistoryIface
{
    Q_OBJECT
public:
    explicit GrblHistoryLogic(GrblEngine *engine, GrblBoard *parent);

public: // QAbstractItemModel interface
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;

signals:
    void messageAddedToItem(int index);

public slots:
    void onInstructionSent(GrblInstructionPointer instPtr);
    void onInstructionCompleted(GrblInstructionPointer instPtr);
    void onMessageReceived(GrblMessage message);

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    struct HistoryItem {
        HistoryItem();
        HistoryItem(GrblInstructionPointer instruction);
        HistoryItem(GrblMessage message);

        GrblInstructionPointer instruction;
        QVector<GrblMessage> messages;
    };

    QList<HistoryItem> m_history;
    int m_capacity;

    void addItemToHistory(HistoryItem item);
    int findItemFromInstruction(GrblInstructionPointer instPtr);

    static const QMap<GrblInstruction::Status,GrblHistoryLogic::Status> s_instructionToItemStatusMap;
};

#endif // GRBLHISTORY_H
