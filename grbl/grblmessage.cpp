#include "grblmessage.h"
#include "grbldefinitions.h"
#include "grblstringregister.h"

const QMap <GrblMessage::Type, GrblMessage::TypeInfoStruct> GrblMessage::s_typeDelimiterMap =
{
    {
        GrblMessage::Type_Ok,                          //Response type
        {
            QStringLiteral(RESPONSE_OK),                //Begins with
            QStringLiteral(),                           //Ends with
            GrblMessage::TypeInfoStruct::RawText,       //Only extract usefull text
            QStringLiteral()                            //How to format end user text for ExtractNumeric type
        }
    },
    {
        GrblMessage::Type_Error,
        {
            QStringLiteral(RESPONSE_ERROR),
            QStringLiteral(),
            GrblMessage::TypeInfoStruct::ExtractNumeric,
            QStringLiteral("Error %1 : %2")
        }
    },
    {
        GrblMessage::Type_Alarm,
        {
            QStringLiteral(RESPONSE_ALARM),
            QStringLiteral(),
            GrblMessage::TypeInfoStruct::ExtractNumeric,
            QStringLiteral("Alarm %1 : %2")
        }
    },
    {
        GrblMessage::Type_Status,
        {
            QStringLiteral(RESPONSE_STATUS_START),
            QStringLiteral(RESPONSE_STATUS_END),
            GrblMessage::TypeInfoStruct::ExtractText,
            QStringLiteral()
        }
    },
    {
        GrblMessage::Type_Feedback,
        {
            QStringLiteral(RESPONSE_FEEDBACK_START),
            QStringLiteral(RESPONSE_FEEDBACK_END),
            GrblMessage::TypeInfoStruct::ExtractText,
            QStringLiteral()
        }
    },
    {
        GrblMessage::Type_Welcome,
        {
            QStringLiteral(GRBL_WELCOME_STRING),
            QStringLiteral(),
            GrblMessage::TypeInfoStruct::RawText,
            QStringLiteral()
        }
    },
    {
        GrblMessage::Type_Parameter,
        {
            QStringLiteral(RESPONSE_PARAM_START),
            QStringLiteral(),
            GrblMessage::TypeInfoStruct::RawText,
            QStringLiteral()
        }
    },
};

GrblMessage::GrblMessage(QString responseString):
    m_type(GrblMessage::Type_Unknown),
    m_text(responseString),
    m_instructionPtr()
{    
    //Iterate through typeDelimiterMap to determine the type of responseString
    for(QMap<Type, TypeInfoStruct>::const_iterator currType = s_typeDelimiterMap.cbegin() ;
        currType != s_typeDelimiterMap.cend();
        currType++)
    {
        //Check if our responseString matches prefix and suffix for the current type
        if(responseString.startsWith(currType->prefix)
        && responseString.endsWith(currType->suffix))
        {
            //We found the type of the response !
            m_type = currType.key();

            //If we keep raw text, no more processing to do
            if(currType->textType == TypeInfoStruct::RawText)
            {
                break;
            }

            //Else we have to separate text from prefix / suffix
            const int prefixLength = currType->prefix.length();
            const int suffixLength = currType->suffix.length();
            const int textLength = responseString.length() - prefixLength - suffixLength;

            //Here we have an issue !
            if(textLength < 0)
            {
                break;
            }

            QString extractedText = responseString.mid(prefixLength, textLength);

            //If we wanted extracted text, no need to go further
            if(currType->textType == TypeInfoStruct::ExtractText)
            {
                m_text = extractedText;
                break;
            }

            //Now we have to translate numeric value to string
            bool isConversionSuccess;
            int value = extractedText.toInt(&isConversionSuccess);

            //Generate final text
            m_text = isConversionSuccess
                ? currType->messageText.arg(extractedText).arg(convertNumericTextToString(value))   //Grbl >= v1.1 : Numeric codes
                : currType->messageText.arg("").arg(extractedText);                                 //Grbl < v1.1 : Plain text codes

            break;
        }
    }
}

bool GrblMessage::isInstructionResponse() const
{
    switch (m_type)
    {
    case GrblMessage::Type_Ok:
    case GrblMessage::Type_Error:
        return true;

    default:
        return false;
    }
}

QString GrblMessage::convertNumericTextToString(quint8 value)
{
    switch(m_type)
    {
    case Type_Alarm:
        return GrblStringRegister::getAlarmString(value);

    case Type_Error:
        return GrblStringRegister::getErrorString(value);

    default:
        return QStringLiteral("Unsupported message type : Check GRBL wiki to get more infos");
    }
}

void GrblMessage::setRelatedInstruction(GrblInstructionPointer instruction)
{
    m_instructionPtr = instruction;
}
