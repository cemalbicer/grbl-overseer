#include "grblbuildinfoslogic.h"

#include "grblengine.h"
#include "grblboard.h"
#include "grbldefinitions.h"

#include <QDebug>

const QString GrblBuildInfosLogic::s_buildVersionString =
        QStringLiteral(GRBL_BUILD_VERSION_STRING);

const QString GrblBuildInfosLogic::s_buildOptionsString =
        QStringLiteral(GRBL_BUILD_OPTIONS_STRING);

const QMap<QChar, GrblBuildInfosIface::BuildOption> GrblBuildInfosLogic::s_buildOptionTranslator =
{
    { GRBL_OPTION_VAR_SPINDLE         , GrblBuildInfosIface::Option_VariableSpindle                  },
    { GRBL_OPTION_LINE_NUMBERS        , GrblBuildInfosIface::Option_LineNumbers                      },
    { GRBL_OPTION_MIST_COOLANT        , GrblBuildInfosIface::Option_MistCoolant                      },
    { GRBL_OPTION_COREXY              , GrblBuildInfosIface::Option_CoreXY                           },
    { GRBL_OPTION_PARKING_MOTION      , GrblBuildInfosIface::Option_ParkingMotion                    },
    { GRBL_OPTION_HOMING_FORCE_ORIGIN , GrblBuildInfosIface::Option_HomingForceOrigin                },
    { GRBL_OPTION_HOMING_SINGLE_AXIS  , GrblBuildInfosIface::Option_HomingSingleAxis                 },
    { GRBL_OPTION_2_LIM_SWITCHES_AXIS , GrblBuildInfosIface::Option_TwoLimitSwitchesOnAxis           },
    { GRBL_OPTION_SPNDL_DIR_PIN_AS_EN , GrblBuildInfosIface::Option_SpindleDirPinAsEnablePin         },
    { GRBL_OPTION_OFF_WHEN_SPEED_0    , GrblBuildInfosIface::Option_SpindleEnableOffWhenSpeedZero    },
    { GRBL_OPTION_SOFT_LIMIT_DEBOUNCE , GrblBuildInfosIface::Option_SoftwareLimitPinDebouncing       },
    { GRBL_OPTION_PARKING_OVR         , GrblBuildInfosIface::Option_ParkingOverrideControl           },
    { GRBL_OPTION_FEED_OVR_IN_PROBING , GrblBuildInfosIface::Option_FeedOverrideInProbeCycles        },
    { GRBL_OPTION_NO_RSTRE_ALL_EEPROM , GrblBuildInfosIface::Option_RestoreAllEepromDisabled         },
    { GRBL_OPTION_NO_RSTRE_$_SETTINGS , GrblBuildInfosIface::Option_RestoreEeprom$SettingsDisabled   },
    { GRBL_OPTION_NO_RSTRE_PARAM_DATA , GrblBuildInfosIface::Option_RestoreEepromParamDataDisabled   },
    { GRBL_OPTION_NO_BUILD_INFO_WRITE , GrblBuildInfosIface::Option_BuildInfoWriteUserStringDisabled },
    { GRBL_OPTION_NO_SYNC_ON_EEPROM_W , GrblBuildInfosIface::Option_ForceSyncOpenEepromWriteDisabled },
    { GRBL_OPTION_NO_SYNC_ON_WCO_CHNG , GrblBuildInfosIface::Option_ForceSyncUponWcoChangeDisabled   },
    { GRBL_OPTION_NO_HOMING_AUTO_LOCK , GrblBuildInfosIface::Option_HomingInitAutoLockDisabled       }
};

GrblBuildInfosLogic::GrblBuildInfosLogic(GrblEngine *engine, GrblBoard *parentBoard) :
    GrblBuildInfosIface(parentBoard),
    m_planBufferDepth(-1),
    m_charBufferDepth(-1)
{
    connect(engine, &GrblEngine::msgReceivedTypeFeedback,
            this, &GrblBuildInfosLogic::onFeedbackMessageReceived);
}

void GrblBuildInfosLogic::onFeedbackMessageReceived(GrblMessage message)
{
    const QString messageString = message.getText();

    //Build version
    if(messageString.startsWith(s_buildVersionString))
    {
        QString newBuildString = messageString.mid(s_buildVersionString.length());

        if(newBuildString != m_buildString)
        {
            m_buildString = newBuildString;
            //qDebug() << "Buildstring changed" << m_buildString;
            emit buildStringChanged(m_buildString);
        }
    }

    //Build options
    else if(messageString.startsWith(s_buildOptionsString))
    {
        //qDebug() << "Options string" << messageString;
        QString optionString = messageString.mid(s_buildOptionsString.length());
        QVector<QStringRef> optionFields = optionString.splitRef(GRBL_BUILD_OPTIONS_SEPARATOR);

        if(parseOptionsField(optionFields.at(0)))
        {
            //qDebug() << "Options changed" << m_buildOptions;
            emit optionsChanged(m_buildOptions);
        }

        if(parseBufferField(optionFields, 1, &m_planBufferDepth))
        {
            emit planBufferDepthChanged(m_planBufferDepth);
        }

        if(parseBufferField(optionFields, 2, &m_charBufferDepth))
        {
            emit charBufferDepthChanged(m_charBufferDepth);
        }
    }
}

bool GrblBuildInfosLogic::parseOptionsField(const QStringRef &field)
{
    BuildOptions parsedBuildOptions(0);

    for(QStringRef::const_iterator i = field.cbegin();
        i != field.cend();
        i++)
    {
        parsedBuildOptions |= s_buildOptionTranslator.value(*i);
    }

    if(parsedBuildOptions != m_buildOptions)
    {
        m_buildOptions = parsedBuildOptions;
        return true;
    }
    return false;
}

bool GrblBuildInfosLogic::parseBufferField(const QVector<QStringRef> &optionFields, int index, int *value_dest)
{
    int newBufDepth = (optionFields.size() >= index+1) ? optionFields.at(index).toInt() : -1;

    if(value_dest && ( (*value_dest) != newBufDepth ) )
    {
        (*value_dest) = newBufDepth;
        return true;
    }
    return false;
}
