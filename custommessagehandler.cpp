#include "custommessagehandler.h"

CustomMessageHandler *CustomMessageHandler::s_handler = NULL;

CustomMessageHandler::CustomMessageHandler(QObject *parent) : QObject(parent)
{

}

void CustomMessageHandler::printMessage(QString message)
{
    emit newMessage(message.append('\n'));
}

CustomMessageHandler *CustomMessageHandler::getHandlerPtr()
{
    if (NULL == s_handler)
    {
        s_handler = new CustomMessageHandler;
    }

    return s_handler;
}

void CustomMessageHandler::MessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(type)
    Q_UNUSED(context)
    getHandlerPtr()->printMessage(msg);
}
