#include <gcode/gcodejobmanager.h>
#include <gcode/gcodejob.h>
#include <grbl/grblhistoryiface.h>
#include <grbl/grblconfigiface.h>
#include <grbl/grblstringregister.h>
#include <grbl/grblboard.h>
#include <grbl/grblstatusiface.h>
#include <grbl/grblbuildinfosiface.h>
#include <link/linkmanager.h>
#include <visualizer/visualizeritem.h>
#include <applicationlogic.h>
#include <custommessagehandler.h>

#ifdef Q_OS_ANDROID
#include <android/keepawakehelper.h>
#endif

#include <QQmlContext>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTime>
#include <QTranslator>
#include <QCommandLineParser>
#include <QDebug>

//#define ENABLE_CUSTOM_MESSAGE_HANDLING


int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    //Initialize RNG
    qsrand(uint(QTime::currentTime().msec()));

    //Parse command line arguments
    QCommandLineParser parser;
    QCommandLineOption setLocaleOption(QStringLiteral("locale"),
                                       QStringLiteral("Specify the locale used for translation."),
                                       QStringLiteral("locale"),
                                       QLocale::c().name());

    QCommandLineOption startFullScreenOption(QStringList() << "f" << "fullscreen",
                                             QStringLiteral("Start the app in fullscreen."));

    QCommandLineOption serialPortOption(QStringList() << "p" << "port",
                                        QStringLiteral("Specify port and optionally baudrate to use."),"port[:baudrate]");

    QCommandLineOption skipSimulationOption(QStringLiteral("skip-simulation"),
                                            QStringLiteral("Directly start production when pressing \"Go\""));

    parser.addOption(setLocaleOption);
    parser.addOption(startFullScreenOption);
    parser.addOption(serialPortOption);
    parser.addOption(skipSimulationOption);
    parser.addHelpOption();

    parser.addPositionalArgument("files", QStringLiteral("Job files to open, optionally"), "[files...]");

    parser.process(a);

    //Load translations
    QTranslator translator;
    translator.load(parser.value(setLocaleOption), QStringLiteral(":/lang"));
    a.installTranslator(&translator);

    //Prepare the file containing GRBL numeric codes definitions
    QString stringDefinitionFilePath(":/grbl_strings.ini");
    GrblStringRegister::loadStringsFile(stringDefinitionFilePath);

    //Instantiate C++ objects
    LinkManager linkManager;
    GrblBoard grblBoard;
    GCodeJobManager jobManager;

    QObject::connect(&linkManager,&LinkManager::linkDeviceChanged,
                     &grblBoard, &GrblBoard::setLinkDevice);
    QObject::connect(grblBoard.configuration(),&GrblConfigIface::seekRatesChanged,
                     &jobManager, &GCodeJobManager::onBoardSeekRateChanged);
    QObject::connect(grblBoard.status(), &GrblStatusIface::machinePositionChanged,
                     &jobManager, &GCodeJobManager::setDefaultJobOrigin);

    ApplicationLogic appLogic(&jobManager, &grblBoard);

    //Command-line specified serial port
    QString specifiedPortString = parser.value(serialPortOption);
    if(!specifiedPortString.isEmpty())
    {
        QStringList argList = specifiedPortString.split(':');
        QString portName = argList.at(0);
        QString baudrateString = (argList.size() > 1) ? argList.at(1) : linkManager.getBaudratesList().at(0);
        qInfo() << "Using specified port " << portName << "at baudrate" << baudrateString;
        linkManager.openLink(portName,baudrateString);
    }

    //Register QML types
    qmlRegisterSingletonType(QUrl("qrc:/qml/Style.qml"), "StyleFile", 1, 0, "Style" );

    qmlRegisterType<VisualizerItem>("OpenGLVisualizerItem", 1, 0, "VisualizerItem");

    qmlRegisterUncreatableType<GCodeJob>
            ("GCodeJob"     , 1 , 0,    "Job",          "This type in only usable to access enums");
    qmlRegisterUncreatableType<GrblBoard>
            ("GrblBoard"   , 1 , 0,    "GrblBoard",     "This type in only usable to access enums");
    qmlRegisterUncreatableType<GrblHistoryIface>
            ("GrblHistory"  , 1 , 0,    "GrblHistory",  "This type in only usable to access enums");
    qmlRegisterUncreatableType<GrblStatusIface>
            ("GrblStatus"   , 1 , 0,    "GrblStatus",   "This type in only usable to access enums");
    qmlRegisterUncreatableType<ApplicationLogic>
            ("AppLogic"     , 1 , 0,    "AppLogic",     "This type in only usable to access enums");


    //Create QML application
    QQmlApplicationEngine engine;

    //Set QML properties
    QQmlContext *ctxt = engine.rootContext();
    ctxt->setContextProperty("jobManager"   , &jobManager);
    ctxt->setContextProperty("linkManager"  , &linkManager);
    ctxt->setContextProperty("grblBoard"    , &grblBoard);
    ctxt->setContextProperty("grblHistory"  , grblBoard.history());
    ctxt->setContextProperty("grblStatus"   , grblBoard.status());
    ctxt->setContextProperty("grblConfig"   , grblBoard.configuration());
    ctxt->setContextProperty("grblBuild"    , grblBoard.buildInfos());
    ctxt->setContextProperty("appLogic"     , &appLogic);

    bool isSkipSimulationSet = parser.isSet(skipSimulationOption);
    ctxt->setContextProperty("skipSimulationEnabled", isSkipSimulationSet);

#ifdef ENABLE_CUSTOM_MESSAGE_HANDLING
    //Install message handling facilities for case when running with debugger is NOT possible
    ctxt->setContextProperty("messageHandler", CustomMessageHandler::getHandlerPtr());
    qInstallMessageHandler(CustomMessageHandler::MessageHandler);

#endif

    engine.load(QUrl("qrc:/qml/main.qml"));

    //Connect jobManager to QML-instanciated Visualizer
    VisualizerItem *visualizer = engine.rootObjects().first()->findChild<VisualizerItem*>("visualizer");

    if (parser.isSet(startFullScreenOption)) {
        QQuickWindow *window = qobject_cast<QQuickWindow*>(engine.rootObjects().first());
        window->setVisibility(QWindow::FullScreen);
    }

    QObject::connect(&jobManager, &GCodeJobManager::jobAdded,
                     visualizer, &VisualizerItem::onJobAdded);
    QObject::connect(&jobManager, &GCodeJobManager::jobRemoved,
                     visualizer, &VisualizerItem::onJobRemoved);
    QObject::connect(&jobManager, &GCodeJobManager::jobLineStatusChanged,
                     visualizer, &VisualizerItem::onJobLineStatusChanged);
    QObject::connect(&jobManager, &GCodeJobManager::jobPositionChanged,
                     visualizer, &VisualizerItem::onJobPositionChanged);
    QObject::connect(&jobManager, &GCodeJobManager::jobColorChanged,
                     visualizer, &VisualizerItem::onJobColorChanged);
    QObject::connect(&jobManager, &GCodeJobManager::selectedJobChanged,
                     visualizer, &VisualizerItem::onSelectedJobChanged);
    QObject::connect(grblBoard.status(), &GrblStatusIface::machinePositionChanged,
                     visualizer, &VisualizerItem::onMachinePositionChanged);
    QObject::connect(grblBoard.configuration(), &GrblConfigIface::machineSpaceChanged,
                     visualizer, &VisualizerItem::onMachineSpaceChanged);
    QObject::connect(&grblBoard, &GrblBoard::homingStateChanged,
                     visualizer, &VisualizerItem::onHomingStateChanged);
    QObject::connect(grblBoard.buildInfos(), &GrblBuildInfosIface::optionsChanged,
                     visualizer, &VisualizerItem::onBuildOptionsChanged);
    QObject::connect(grblBoard.status(), &GrblStatusIface::accessoryStateChanged,
                     visualizer, &VisualizerItem::onAccessoryStateChanged);
    QObject::connect(grblBoard.configuration(), &GrblConfigIface::reportUnitIsInchChanged,
                     visualizer, &VisualizerItem::onReportImperialUnitsChanged);

#ifdef Q_OS_ANDROID
    KeepAwakeHelper screenAwakeLocker;  //Device won't go in standby
#endif


    //Command-line specified jobs
    const QStringList jobPathList = parser.positionalArguments();
    for(auto jobPath = jobPathList.cbegin() ; jobPath != jobPathList.cend() ; jobPath++)
    {
        jobManager.addJob(*jobPath);
    }

    return a.exec();
}
