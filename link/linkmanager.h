#ifndef LINKMANAGER_H
#define LINKMANAGER_H

#include <QObject>
#include <QTimer>
#include <QMap>

class QIODevice;
class GrblBoard;

class LinkManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList portList READ getPortList NOTIFY portListUpdated)
    Q_PROPERTY(QStringList baudratesList READ getBaudratesList NOTIFY baudRateListUpdated)
    Q_PROPERTY(bool linkOpen READ isLinkOpen NOTIFY linkOpenChanged)

public:
    explicit LinkManager(QObject *parent = nullptr);

    bool isLinkOpen();

    QStringList getPortList() const
        {return m_availablePortList;}

    QStringList getBaudratesList() const
        {return m_baudrateList;}

signals:
    void portListUpdated();
    void baudRateListUpdated();
    void linkOpenChanged(bool linkState);

    void linkDeviceChanged(QIODevice* linkDevice);

public slots:
    void openLink(QString portName, QString baudRate);
    void closeLink();

private slots:
    void rescanAvailablePorts();

private:
    enum PortType{TypeQSerial};

    QTimer *m_scanTimer;
    QStringList m_availablePortList;
    QStringList m_baudrateList;

    QIODevice* m_linkDevice;

    static const QStringList s_standardBaudratesList;
};

#endif // LINKMANAGER_H
